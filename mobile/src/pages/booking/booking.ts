import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Loading } from 'ionic-angular';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import * as moment from 'moment';
import { ConstantProvider } from '../../providers/constant/constant';
import { HttpProvider } from '../../providers/http/http';

@IonicPage({
  name: 'booking'
})
@Component({
  selector: 'page-booking',
  templateUrl: 'booking.html',
})
export class BookingPage {
  loading: Loading;
  customer_id: number;
  id: number = 0;
  startDate: Date;
  bookingForm: FormGroup;
  roomTypes: any = [];
  today = moment().format("YYYY-MM-DD");
  commingSevenDay: string;
  fromMax: string; 
  toMax: string;

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public formBuilder: FormBuilder,
    public constantProvider: ConstantProvider,
    public httpProvider: HttpProvider,) {
      this.id = this.navParams.data;
      this.httpProvider.post('/room/call_helper',{function:'room_type'}).subscribe((data)=>{
        let res=data.json();
        this.roomTypes=res;
      });
      if(this.id > 0) {
        this.httpProvider.get("booking/booking/"+ this.id).subscribe((data) => {
          let res = data.json();
            if(!res.error) {
              let data = res.data;
              this.buildForm();
              this.bookingForm.patchValue(data);
            }
        });
      }
      let seven = moment().add(7, 'days').calendar();
      this.commingSevenDay = moment(seven).format("YYYY-MM-DD");
      let from_max = moment().add(60, 'days').calendar();
      this.fromMax = moment(from_max).format("YYYY-MM-DD");
      this.customer_id = parseInt(localStorage.getItem('id'));
      this.buildForm();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad BookingPage');
  }

  buildForm() {
    this.bookingForm = this.formBuilder.group({
      start_date: ['', Validators.required],
      end_date: ['', Validators.required],
      room_type: ['', Validators.required],
      room_no: ['', Validators.required],
      adult_no: ['', Validators.required],
      children_no: ['']
    });
  }

  setStartDate() {
    let max = moment(this.startDate).add(60, 'days').calendar();
    this.toMax = moment(max).format("YYYY-MM-DD");
  }

  onSubmit({value, valid}) {
    if(valid) {
      this.loading = this.constantProvider.loading();
      if(this.bookingForm.value.start_date >= this.bookingForm.value.end_date) {
        this.loading.dismiss().then(() => {
          this.constantProvider.basicAlert("Error!", "Booking start date exceed more than end date.");
        });
      } else {
        let parameter : any = {};
        parameter = value;
        parameter.customer_id = this.customer_id;
        if(this.id > 0) {
          let rt : any = this.httpProvider.put("booking/booking/"+ this.id, parameter);
          rt.subscribe((data) =>{
            let res = data.json();
            this.loading.dismiss().then(() => {
              this.constantProvider.toast("Successfully Modify Booking Request!");
              localStorage.setItem("id",res.data.customer_id);
              this.navCtrl.push("confirm");
            })          
          });
        } else {
          let rt : any = this.httpProvider.post("booking/booking",parameter);
          rt.subscribe((data) =>{
            let res = data.json();
            this.loading.dismiss().then(() => {
              this.constantProvider.toast("Successfully Send Booking Request!");
              localStorage.setItem("id",res.data.customer_id);
              this.navCtrl.push("confirm");
            })          
          });
        }
      }     
    }
  }

}
