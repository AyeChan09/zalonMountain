<?php namespace App\Library;
use Curl;
use Modules\Auth\Entities\User;
use Modules\Auth\Entities\UserSession;
use Carbon\Carbon;
class arUser{
    function login($username = "",$password = "",$system){
        $return = array();
        $auth['username'] = $username;
        $auth['password'] = $password;
        $auth['system'] = $system;
        $response = Curl::to('http://localhost:8888/auth/login')
        ->withData( $auth )
        ->asJson()
        ->post();
        if(!$response->error){
            $userData = $response->user_data;
            $user = User::where("code",$userData->code)->first();
            if(!isset($user->id)){
                $user = new User;
            }
            $user->code = $userData->code;
            $user->full_name = $userData->full_name;
            $user->surname = $userData->surname;
            $user->email = $userData->email;
            $user->mobile = $userData->mobile;
            $user->join_date = $userData->join_date;
            $user->last_login = $userData->last_login;
            $user->save();
            $flag = 0;
            if($flag == 0){
                $rand = str_random(50);
                $session = UserSession::where("token",$rand);
                if(!isset($session->id)){
                    $session = new UserSession;
                    $session->user_id = $user->id;
                    $session->token = $rand;
                    $session->login_time = Carbon::now();
                    $session->save();
                    $flag = 1;
                }
            }
            $return['token'] = $rand;
            $return['error'] = false;
            $return['msg'] = "[[authentication_success]]";
        }else{
            $return['error'] = true;
            $return['msg'] = "[[wrong_username_or_password]]";
        }
        return $return;
    }
}