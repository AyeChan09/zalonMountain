<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Schema;
use Hashids;
class ARmodel extends Model
{
    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at'
    ];
    protected $casts = [
        'id'=>'string',
    ];
    protected $foreign_key = [];
    protected $filterSelectorLabel = "name";
    protected $filterSelectorId = "name";
    
    public function hasMany($related, $foreignKey = NULL, $localKey = NULL){
        if(!is_numeric($this->id)){
            $this->id = $this->decodeId($this->id);
        }
        return parent::hasMany($related,$foreignKey,$localKey);
    }
    public static function find($value,$key = "id",$encrypt = 1){
    
        $self = new static;
        if($value != "" && !is_numeric($value) && ($key == "id" || in_array($key,$self->foreign_key) )){
          
            $return = parent::where($key,$self->decodeId($value))->first();
        
        }else{
         
            $return = parent::where($key,$value)->first();
        }
        if($encrypt == 1){
            return $self->transformKeyRecord($return);
        }else{
            return $return;
        }
    }
    
    public static function boot()
    {   
        $self = new static;
        $foreign_key = $self->foreign_key;
        static::creating(function ($model) {
            $user_name = session("user_name");
            if($model->company_id == "" && $model->getTable() != 'user'){
                if(Schema::hasColumn($model->getTable(), 'company_id')){ 
                    $model->company_id = session("company_id");
                }
            }
            if(!isset($model->created_by)){ 
                if($user_name != ""){
                    $model->created_by = $user_name;
                }else{
                    $model->created_by = "guest";
                }
            }
    
        });

        static::updating(function ($model) {
            $user_name = session("user_name");
            if(!isset($model->updated_by)){
                if($user_name != ""){
                    $model->updated_by = $user_name;
                }else{
                    $model->updated_by = "guest";
                }
            }
        });

        static::deleting(function ($model) {
            
        });
        static::saving(function ($model) use ($foreign_key, $self){
         
            if($model->id != "" && !is_numeric($model->id)){
                $model->id =  $self->decodeId($model->id);
            }
            if(count($foreign_key) > 0){
                foreach($foreign_key as $key){
                    if($model->$key != "" && !is_numeric($model->$key)){
                        $model->$key = $self->decodeId($model->$key);
                    }
                }
            }
            
        });
        parent::boot();
    }
    public function decodeId($value){
        $return = Hashids::decode($value);
        if(isset($return[0])){
            return $return[0];
        }else{
            return $value;
        }
    }
    
    public function selector($limited = "no",$page = 0,$limit = "10",$searchValue = "",$searchKey = []){
        $company_id = session("company_id");
        
        $data = $this->where("deleted_at",null);
 
        if(Schema::hasColumn($this->getTable(),"company_id") && $this->getTable() != 'user'){
           $data->where("company_id",$company_id);
        }

        if($limited == "no"){
            $return =  $data->get();
        }else{
            foreach($searchKey as $key){
                $data->orWhere($key,$searchValue);
            }
            $return =  $data->offset($page)->limit($limit)->get();
        }
        return $this->transformKeyCollection($return);
    }
    public function filterSelector($label,$value){
        $company_id = session("company_id");

        $data = $this->where("deleted_at",null);
        if(Schema::hasColumn($this->getTable(),"company_id") && $this->getTable() != 'user' && $this->getTable() != 'project_work_order_chat'){
            $data->where("company_id",$company_id);
        }
        return $data->select("$value as value","$label as label")->get();
    }
    public function datatable($page = 0,$limit = 10,$filterValue = "",$filterFields = [] ,$searchArray = [],$order = [],$primary="id"){
        $company_id = session("company_id");
        $data = $this->where("deleted_at",null);
        if(Schema::hasColumn($this->getTable(),"company_id") && $this->getTable() != 'user' && $this->getTable() != 'project_work_order_chat'){
           $data->where("company_id",$company_id);
        }
        if($filterValue != ""){
            $data->where(function($query) use ($filterValue,$filterFields){
                foreach($filterFields as $field){
                    $query->orWhere($field,"like","%$filterValue%");
                }
            });
        }
        foreach($searchArray as $key=>$value){
            if($value==''){
                continue;
            }
            if(strpos($key,"_date_operator") == true || strpos($key,"_date_value") == true){
                if(strpos($key,"_date_value") == true && $value != ""){
                    $dateKey = str_replace("_date_value","",$key);
                    $data->where($dateKey,$searchArray[$dateKey."_date_operator"],$value);
                }
            }else{
                if(in_array($key,$this->foreign_key)){
                    if(!is_numeric($value)){
                        $data->where($key,$this->decodeId($value));   
                    }else{                    
                        $data->where($key,$value);                           
                    }

                }else{
                    $data->where($key,$value);   
                }
            }
        }
        $count = $data->count($primary);

        if(isset($order['by']) && $order['by'] != ""){
            $order_dir = $order['dir'] == "" ? "desc" : $order['dir'];
            $data->orderBy($order['by'], $order_dir);
        }
        $return['query'] = $data->toSql();
        $data = $data->offset(($page)*$limit)->limit($limit)->get();
        $return['data'] = $this->transformKeyCollection($data);
        $return['count'] = $count;
        return $return;
    }
    public function toDatatable($data,$page = 0,$limit = 10,$order = [],$primary="id"){
        $company_id = session("company_id");
        
        if(Schema::hasColumn($this->getTable(),"company_id") && $this->getTable() != 'user' && $this->getTable() != 'project_work_order_chat'){
           $data->where("company_id",$company_id);
        }
        
        $count = $data->count($primary);

        if(isset($order['by']) && $order['by'] != ""){
            $order_dir = $order['dir'] == "" ? "desc" : $order['dir'];
            $data->orderBy($order['by'], $order_dir);
        }
        $return['query'] = $data->toSql();
        $data = $data->offset(($page)*$limit)->limit($limit)->get();
        $return['data'] = $this->transformKeyCollection($data);
        $return['count'] = $count;
        return $return;
    }
    
    public function get($encrypt = 1){
        $return = parent::get();
        if($encrypt == 1){
            return $this->transformKeyCollection($return);
        }else{
            return $return;
        }
    }
    public function first(){
        $return = parent::first();
        return $self->transformKeyRecord($return);
    }
    public function getEnumValues( $column)
    {
        $table = $this->table_name;
        $type = DB::select( DB::raw("SHOW COLUMNS FROM $table WHERE Field = '$column'") )[0]->Type;
        preg_match('/^enum\((.*)\)$/', $type, $matches);
        $enum = array();
        foreach( explode(',', $matches[1]) as $value )
        {
            $enum[] = trim( $value, "'" );
        }
        return $enum;
    }
    public function transformKeyRecord($item){
       
        if(isset($item->id)){
        $item->id = Hashids::encode($item->id);
        }

        foreach($this->foreign_key as $key){
            if(isset($item->$key)){
                $item->$key = Hashids::encode($item->$key);
            }
        }
        return $item;
    }
    public function transformKeyCollection($collection){
        return $collection->transform(function($item,$key){
            $item->id = Hashids::encode($item->id);
            foreach($this->foreign_key as $key){
                $item->$key = Hashids::encode($item->$key);
            }
            return $item;
        });
    }
    public static function __callStatic($method, $parameters) {

        if (is_callable($method))
        {
            return call_user_func(array($method, $parameters));
        }else{
            return parent::__callStatic($method, $parameters);
        }
    }
}
