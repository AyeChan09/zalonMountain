<?php

namespace Modules\Cash\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

use Modules\Cash\Entities\Cash;
use Modules\Booking\Entities\Booking;

class CashController extends Controller
{
    private $entity;
    public function __construct(){
        $this->entity = new Cash;
    }
    
    public function index(Request $request)
    {
        $input=$request->input();
        $page=$input['page'];
        $limit=$input['limit'];
        $data = $this->entity::select('cash.*', 'customer.name as customer')
                ->where('status', 'uncheck')
                ->leftJoin('customer', 'cash.customer_id', 'customer.id')
                ->offset(($page-1)*$limit)->limit($limit); 
        
        $totalData = $data->count("cash.id");
        $data=$data->get();
        $return['total'] = $totalData;
        $return['data'] = $data;
        return $return;
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('cash::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $input = $request->input();
        $data = $this->entity->where('booking_id', $input['booking_id'])->first();
        if($data) {
            $data = $this->entity->where('id', $data['id'])->update(['phone_no'=>$input['phone_no'], 'password'=>$input['password'], 'status'=>'uncheck']);
        } else {
            $data = $this->entity;
            foreach($input as $key=>$value){
                if(Schema::hasColumn($data->getTable(), $key)){
                    $data->$key=$value;
                }
            }
            $data->status = 'uncheck';
            $data->save();
        }     
        $return['data'] = $data;
        $return['err'] = false;
        $return['message'] = 'successful'; 
        return $return;
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('cash::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        return view('cash::edit');
    }

    public function update(Request $request, $id)
    {
        $return['data'] = $this->entity->where('id', $id)->update(['status'=>'receive']);
        $bookingId = $this->entity->where('id', $id)->first()->booking_id;
        Booking::where('id', $bookingId)->update(['status' => 'approve']);
        $return['error'] = false;
        return $return;
    }

    public function wrongData(Request $request, $id)
    {
        $return['data'] = $this->entity->where('id', $id)->update(['status'=>'wrong']);
        return $return;
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy()
    {
    }
}
