import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions} from '@angular/http';//HttpModule,RequestMethod,Response,URLSearchParams
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/delay';
import { ConstantProvider } from '../constant/constant';

@Injectable()
export class HttpProvider {
  public AppConfig: any;
  public token : string = "";
  apiPath = "";
  constructor(
    private http:Http,
    private constantProvider : ConstantProvider) {
    this.apiPath = this.constantProvider.getApiPath();
  }
  get(url, parameter = {}, headers = {}) {
    parameter['Authorization'] = localStorage.getItem('ibeautiUserToken');
    if (headers['Content-Type'] !== "undefined") {
      headers['Content-Type'] = "application/json"
    }
    let httpHeader = new Headers(headers);
    let options = new RequestOptions({ headers: httpHeader, params: parameter });
    return this.http.get(this.apiPath + url, options);

  }
  post(url, parameter = {}, headers = {}) {
    parameter['Authorization'] = localStorage.getItem('ibeautiUserToken');
    if (headers['Content-Type'] !== "undefined") {
      headers['Content-Type'] = "application/json"
    }
    let httpHeader = new Headers(headers);
    let options = new RequestOptions({ headers: httpHeader });

    return this.http.post(this.apiPath + url, parameter, options);


  }
  put(url, parameter = {}, headers = {}) {
    parameter['Authorization'] = localStorage.getItem('ibeautiUserToken');
    if (headers['Content-Type'] !== "undefined") {
      headers['Content-Type'] = "application/json"
    }
    let httpHeader = new Headers(headers);
    let options = new RequestOptions({ headers: httpHeader });

    return this.http.put(this.apiPath + url, parameter, options);


  }
  delete(url, parameter = {}, headers = {}) {
    parameter['Authorization'] = localStorage.getItem('ibeautiUserToken');
    if (headers['Content-Type'] !== "undefined") {
      headers['Content-Type'] = "application/json"
    }
    let httpHeader = new Headers(headers);
    let options = new RequestOptions({ headers: httpHeader });
    // url = url + "?Authorization="+localStorage.getItem('token');
    url = url + "?Authorization=" + localStorage.getItem('ibeautiUserToken');
    return this.http.delete(this.apiPath + url, options);
  }
  newPage(url, parameter = {}) {
    parameter['Authorization'] = localStorage.getItem('ibeautiUserToken');
    let params = new URLSearchParams();
    parameter['search'] = JSON.stringify(parameter['search']);
    parameter['order'] = JSON.stringify(parameter['order']);

    for (let key in parameter) {
      params.set(key, parameter[key]);
    }

    let target = this.apiPath + url;
    var tabWindowId = window.open(target + "?" + params.toString(), '_blank');
  }
  checkToken() {
    let headers: any = { "Content-Type": "application/json" };
    let httpHeader = new Headers(headers);
    let options = new RequestOptions({ headers: httpHeader });

    this.http.post(this.apiPath + "/auth/session_check", { "Authorization": localStorage.getItem('ibeautiUserToken') }, options).subscribe((res) => {
      let ret = res.json();
      if (ret.error) {
      }
    });
  }

}
