<?php

namespace Modules\Generic\Entities;

use Illuminate\Database\Eloquent\Model;
use App\BaseModel;
class LangPresentation extends BaseModel
{
    protected $fillable = [];
    protected $table = "lang_presentation";
}
