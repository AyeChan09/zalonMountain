-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.1.32-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win32
-- HeidiSQL Version:             9.5.0.5196
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for zalon_mountain
CREATE DATABASE IF NOT EXISTS `zalon_mountain` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `zalon_mountain`;

-- Dumping structure for table zalon_mountain.acc_admin_menu_item
CREATE TABLE IF NOT EXISTS `acc_admin_menu_item` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL DEFAULT '',
  `is_show` tinyint(4) DEFAULT NULL,
  `path` varchar(255) NOT NULL DEFAULT '',
  `external_href` varchar(255) DEFAULT NULL,
  `external_href_target` varchar(255) DEFAULT NULL,
  `icon` varchar(255) NOT NULL DEFAULT '',
  `class` varchar(255) NOT NULL DEFAULT '',
  `order_id` int(11) DEFAULT NULL,
  `hasChild` tinyint(1) DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `updated_by` varchar(255) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `deleted_by` varchar(255) DEFAULT NULL,
  `special_data` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- Dumping data for table zalon_mountain.acc_admin_menu_item: ~2 rows (approximately)
/*!40000 ALTER TABLE `acc_admin_menu_item` DISABLE KEYS */;
INSERT INTO `acc_admin_menu_item` (`id`, `title`, `is_show`, `path`, `external_href`, `external_href_target`, `icon`, `class`, `order_id`, `hasChild`, `parent_id`, `created_at`, `created_by`, `updated_at`, `updated_by`, `deleted_at`, `deleted_by`, `special_data`) VALUES
	(1, 'Booking Check', 1, '/synapse/checking/booking', NULL, NULL, 'fa fa-bookmark', '', 1, 0, 0, '2018-08-09 23:11:02', NULL, NULL, NULL, NULL, NULL, NULL),
	(2, 'Cash Check', 1, '/synapse/checking/cash', NULL, NULL, 'fa fa-money', '', 4, 0, 0, '2018-08-09 23:11:02', NULL, NULL, NULL, NULL, NULL, NULL);
/*!40000 ALTER TABLE `acc_admin_menu_item` ENABLE KEYS */;

-- Dumping structure for table zalon_mountain.acc_admin_menu_item_option
CREATE TABLE IF NOT EXISTS `acc_admin_menu_item_option` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `acc_admin_menu_item_id` int(11) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `updated_by` varchar(255) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `deleted_by` varchar(255) DEFAULT NULL,
  `special_data` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- Dumping data for table zalon_mountain.acc_admin_menu_item_option: ~3 rows (approximately)
/*!40000 ALTER TABLE `acc_admin_menu_item_option` DISABLE KEYS */;
INSERT INTO `acc_admin_menu_item_option` (`id`, `acc_admin_menu_item_id`, `name`, `created_at`, `created_by`, `updated_at`, `updated_by`, `deleted_at`, `deleted_by`, `special_data`) VALUES
	(1, 1, 'allow_add', '2018-08-09 23:14:09', NULL, NULL, NULL, NULL, NULL, NULL),
	(2, 1, 'allow_edit', '2018-08-09 23:14:09', NULL, NULL, NULL, NULL, NULL, NULL),
	(3, 1, 'allow_delete', '2018-08-09 23:14:09', NULL, NULL, NULL, NULL, NULL, NULL);
/*!40000 ALTER TABLE `acc_admin_menu_item_option` ENABLE KEYS */;

-- Dumping structure for table zalon_mountain.acc_list_setting
CREATE TABLE IF NOT EXISTS `acc_list_setting` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `list_name` varchar(255) DEFAULT NULL,
  `label` varchar(255) DEFAULT '',
  `field_name` varchar(255) DEFAULT NULL,
  `type` enum('string','currency','date') DEFAULT NULL,
  `is_show` tinyint(4) DEFAULT NULL,
  `order_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `updated_by` varchar(255) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `deleted_by` varchar(255) DEFAULT NULL,
  `special_data` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

-- Dumping data for table zalon_mountain.acc_list_setting: ~10 rows (approximately)
/*!40000 ALTER TABLE `acc_list_setting` DISABLE KEYS */;
INSERT INTO `acc_list_setting` (`id`, `list_name`, `label`, `field_name`, `type`, `is_show`, `order_id`, `created_at`, `created_by`, `updated_at`, `updated_by`, `deleted_at`, `deleted_by`, `special_data`) VALUES
	(1, 'booking', 'Customer Name', 'customer_name', 'string', 1, 1, '2018-08-15 23:09:20', NULL, NULL, NULL, NULL, NULL, NULL),
	(2, 'booking', 'Room Type', 'room_type', 'string', 1, 2, '2018-08-15 23:09:20', NULL, NULL, NULL, NULL, NULL, NULL),
	(3, 'booking', 'No of Room', 'room_no', 'string', 1, 3, '2018-08-15 23:09:20', NULL, NULL, NULL, NULL, NULL, NULL),
	(4, 'booking', 'No of Adult', 'adult_no', 'string', 1, 4, '2018-08-15 23:09:20', NULL, NULL, NULL, NULL, NULL, NULL),
	(5, 'booking', 'No of Children', 'children_no', 'string', 1, 5, '2018-08-15 23:09:20', NULL, NULL, NULL, NULL, NULL, NULL),
	(6, 'room', 'Room No', 'room_no', 'string', 1, 1, '2018-08-15 23:09:20', NULL, NULL, NULL, NULL, NULL, NULL),
	(7, 'room', 'Room Type', 'room_type', 'string', 1, 2, '2018-08-15 23:09:20', NULL, NULL, NULL, NULL, NULL, NULL),
	(8, 'room', 'Status', 'status', 'string', 1, 3, '2018-08-19 01:30:08', NULL, NULL, NULL, NULL, NULL, NULL),
	(9, 'cash', 'Booking', 'booking_id', 'string', 1, 1, '2018-08-19 01:30:08', NULL, NULL, NULL, NULL, NULL, NULL),
	(10, 'cash', 'Customer', 'customer', 'string', 1, 2, '2018-08-19 01:30:08', NULL, NULL, NULL, NULL, NULL, NULL),
	(11, 'cash', 'Phone', 'phone_no', 'string', 1, 3, '2018-08-19 01:30:08', NULL, NULL, NULL, NULL, NULL, NULL),
	(12, 'cash', 'Password', 'password', 'string', 1, 4, '2018-08-19 01:30:08', NULL, NULL, NULL, NULL, NULL, NULL);
/*!40000 ALTER TABLE `acc_list_setting` ENABLE KEYS */;

-- Dumping structure for table zalon_mountain.booking
CREATE TABLE IF NOT EXISTS `booking` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_id` int(11) DEFAULT NULL,
  `accept_person_id` int(11) DEFAULT NULL,
  `start_date` date DEFAULT NULL,
  `end_date` date DEFAULT NULL,
  `room_no` int(11) DEFAULT NULL,
  `room_type` int(11) DEFAULT NULL,
  `adult_no` int(11) DEFAULT NULL,
  `children_no` int(11) DEFAULT NULL,
  `status` enum('waiting','pending','approve') DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(255) DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- Dumping data for table zalon_mountain.booking: ~6 rows (approximately)
/*!40000 ALTER TABLE `booking` DISABLE KEYS */;
INSERT INTO `booking` (`id`, `customer_id`, `accept_person_id`, `start_date`, `end_date`, `room_no`, `room_type`, `adult_no`, `children_no`, `status`, `created_at`, `created_by`, `updated_at`, `updated_by`, `deleted_at`, `deleted_by`) VALUES
	(1, 6, NULL, '2018-08-25', '2018-08-27', 2, 1, 2, 1, 'approve', '2018-07-28 07:02:45', 'guest', '2018-08-08 17:42:06', 'guest', NULL, NULL),
	(2, 7, NULL, '2018-08-07', '2018-08-09', NULL, 2, 6, NULL, 'pending', '2018-07-28 07:16:42', 'guest', '2018-08-23 15:44:24', NULL, NULL, NULL),
	(3, 8, NULL, '2018-05-17', '2018-05-17', 1, 3, 2, NULL, 'pending', '2018-08-08 17:48:28', 'guest', '2018-08-22 19:14:48', NULL, NULL, NULL),
	(4, 8, NULL, '2018-08-23', '2018-08-24', 1, 2, 2, 2, 'waiting', '2018-08-08 17:50:50', 'guest', '2018-08-23 15:43:29', 'guest', NULL, NULL),
	(5, 9, NULL, '2018-09-04', '2018-09-06', 2, NULL, 4, NULL, 'approve', '2018-08-24 18:06:20', 'guest', '2018-08-24 18:06:20', NULL, NULL, NULL),
	(6, 10, NULL, '2018-09-24', '2018-09-25', 1, NULL, 1, NULL, 'approve', '2018-08-24 19:18:20', 'guest', '2018-08-24 19:18:20', NULL, NULL, NULL);
/*!40000 ALTER TABLE `booking` ENABLE KEYS */;

-- Dumping structure for table zalon_mountain.booking_room
CREATE TABLE IF NOT EXISTS `booking_room` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `room_id` int(11) DEFAULT NULL,
  `room_type_id` int(11) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(255) DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=39 DEFAULT CHARSET=utf8;

-- Dumping data for table zalon_mountain.booking_room: ~35 rows (approximately)
/*!40000 ALTER TABLE `booking_room` DISABLE KEYS */;
INSERT INTO `booking_room` (`id`, `room_id`, `room_type_id`, `date`, `created_at`, `created_by`, `updated_at`, `updated_by`, `deleted_at`, `deleted_by`) VALUES
	(1, 1, 1, '2018-08-16', '2018-08-16 22:05:36', NULL, NULL, NULL, NULL, NULL),
	(2, 1, 1, '2018-08-17', '2018-08-16 22:05:36', NULL, NULL, NULL, NULL, NULL),
	(3, 2, 1, '2018-08-17', '2018-08-16 22:05:36', NULL, NULL, NULL, NULL, NULL),
	(4, 6, 2, '2018-08-17', '2018-08-16 22:05:36', NULL, NULL, NULL, NULL, NULL),
	(5, 12, 2, '2018-08-07', '2018-08-22 13:33:10', 'guest', '2018-08-22 13:33:10', NULL, NULL, NULL),
	(6, 12, 2, '2018-08-08', '2018-08-22 13:33:10', 'guest', '2018-08-22 13:33:10', NULL, NULL, NULL),
	(7, 12, 2, '2018-08-09', '2018-08-22 13:33:10', 'guest', '2018-08-22 13:33:10', NULL, NULL, NULL),
	(8, 20, 2, '2018-08-23', '2018-08-22 17:04:35', 'guest', '2018-08-22 17:04:35', NULL, NULL, NULL),
	(9, 20, 2, '2018-08-24', '2018-08-22 17:04:35', 'guest', '2018-08-22 17:04:35', NULL, NULL, NULL),
	(10, 21, 2, '2018-08-07', '2018-08-22 17:08:02', 'guest', '2018-08-22 17:08:02', NULL, NULL, NULL),
	(11, 21, 2, '2018-08-08', '2018-08-22 17:08:02', 'guest', '2018-08-22 17:08:02', NULL, NULL, NULL),
	(12, 21, 2, '2018-08-09', '2018-08-22 17:08:02', 'guest', '2018-08-22 17:08:02', NULL, NULL, NULL),
	(13, 20, 2, '2018-08-23', '2018-08-22 18:16:59', 'guest', '2018-08-22 18:16:59', NULL, NULL, NULL),
	(14, 20, 2, '2018-08-24', '2018-08-22 18:16:59', 'guest', '2018-08-22 18:16:59', NULL, NULL, NULL),
	(15, 13, 2, '2018-08-23', '2018-08-22 18:28:40', 'guest', '2018-08-22 18:28:40', NULL, NULL, NULL),
	(16, 13, 2, '2018-08-24', '2018-08-22 18:28:40', 'guest', '2018-08-22 18:28:40', NULL, NULL, NULL),
	(17, 20, 2, '2018-08-23', '2018-08-22 18:31:48', 'guest', '2018-08-22 18:31:48', NULL, NULL, NULL),
	(18, 20, 2, '2018-08-24', '2018-08-22 18:31:48', 'guest', '2018-08-22 18:31:48', NULL, NULL, NULL),
	(19, 26, 2, '2018-08-07', '2018-08-22 18:34:03', 'guest', '2018-08-22 18:34:03', NULL, NULL, NULL),
	(20, 26, 2, '2018-08-08', '2018-08-22 18:34:03', 'guest', '2018-08-22 18:34:03', NULL, NULL, NULL),
	(21, 26, 2, '2018-08-09', '2018-08-22 18:34:03', 'guest', '2018-08-22 18:34:03', NULL, NULL, NULL),
	(22, 7, 2, '2018-08-07', '2018-08-22 19:02:00', 'guest', '2018-08-22 19:02:00', NULL, NULL, NULL),
	(23, 7, 2, '2018-08-08', '2018-08-22 19:02:00', 'guest', '2018-08-22 19:02:00', NULL, NULL, NULL),
	(24, 7, 2, '2018-08-09', '2018-08-22 19:02:00', 'guest', '2018-08-22 19:02:00', NULL, NULL, NULL),
	(25, 19, 2, '2018-08-23', '2018-08-22 19:05:59', 'guest', '2018-08-22 19:05:59', NULL, NULL, NULL),
	(26, 19, 2, '2018-08-24', '2018-08-22 19:06:00', 'guest', '2018-08-22 19:06:00', NULL, NULL, NULL),
	(27, 19, 2, '2018-08-07', '2018-08-22 19:13:20', 'guest', '2018-08-22 19:13:20', NULL, NULL, NULL),
	(28, 19, 2, '2018-08-08', '2018-08-22 19:13:21', 'guest', '2018-08-22 19:13:21', NULL, NULL, NULL),
	(29, 19, 2, '2018-08-09', '2018-08-22 19:13:21', 'guest', '2018-08-22 19:13:21', NULL, NULL, NULL),
	(30, 24, 3, '2018-05-17', '2018-08-22 19:14:48', 'guest', '2018-08-22 19:14:48', NULL, NULL, NULL),
	(31, 14, 2, '2018-08-07', '2018-08-23 15:40:54', 'guest', '2018-08-23 15:40:54', NULL, NULL, NULL),
	(32, 14, 2, '2018-08-08', '2018-08-23 15:40:54', 'guest', '2018-08-23 15:40:54', NULL, NULL, NULL),
	(33, 14, 2, '2018-08-09', '2018-08-23 15:40:54', 'guest', '2018-08-23 15:40:54', NULL, NULL, NULL),
	(34, 7, 2, '2018-08-23', '2018-08-23 15:43:29', 'guest', '2018-08-23 15:43:29', NULL, NULL, NULL),
	(35, 7, 2, '2018-08-24', '2018-08-23 15:43:29', 'guest', '2018-08-23 15:43:29', NULL, NULL, NULL),
	(36, 13, 2, '2018-08-07', '2018-08-23 15:44:24', 'guest', '2018-08-23 15:44:24', NULL, NULL, NULL),
	(37, 13, 2, '2018-08-08', '2018-08-23 15:44:24', 'guest', '2018-08-23 15:44:24', NULL, NULL, NULL),
	(38, 13, 2, '2018-08-09', '2018-08-23 15:44:24', 'guest', '2018-08-23 15:44:24', NULL, NULL, NULL);
/*!40000 ALTER TABLE `booking_room` ENABLE KEYS */;

-- Dumping structure for table zalon_mountain.cash
CREATE TABLE IF NOT EXISTS `cash` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `booking_id` int(11) DEFAULT NULL,
  `customer_id` int(11) DEFAULT NULL,
  `phone_no` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `status` enum('uncheck','receive') DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(255) DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- Dumping data for table zalon_mountain.cash: ~4 rows (approximately)
/*!40000 ALTER TABLE `cash` DISABLE KEYS */;
INSERT INTO `cash` (`id`, `booking_id`, `customer_id`, `phone_no`, `password`, `status`, `created_at`, `created_by`, `updated_at`, `updated_by`, `deleted_at`, `deleted_by`) VALUES
	(1, 1, 6, '09796786765', '123456', 'receive', NULL, NULL, '2018-08-23 15:45:13', NULL, NULL, NULL),
	(2, 2, 7, '09795643456', 'qwe123', 'uncheck', NULL, NULL, '2018-08-22 19:16:38', NULL, NULL, NULL),
	(3, 5, 9, '09799455996', '1234567', 'receive', '2018-08-24 18:56:45', 'guest', '2018-08-24 18:56:45', NULL, NULL, NULL),
	(4, 6, 10, '09568767', '454665', 'receive', '2018-08-24 19:19:11', 'guest', '2018-08-24 19:19:11', NULL, NULL, NULL);
/*!40000 ALTER TABLE `cash` ENABLE KEYS */;

-- Dumping structure for table zalon_mountain.check_room_available
CREATE TABLE IF NOT EXISTS `check_room_available` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `room_id` int(11) DEFAULT NULL,
  `room_type_id` int(11) DEFAULT NULL,
  `status` tinyint(2) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(255) DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table zalon_mountain.check_room_available: ~0 rows (approximately)
/*!40000 ALTER TABLE `check_room_available` DISABLE KEYS */;
/*!40000 ALTER TABLE `check_room_available` ENABLE KEYS */;

-- Dumping structure for table zalon_mountain.customer
CREATE TABLE IF NOT EXISTS `customer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `nrc` varchar(255) DEFAULT NULL,
  `birth_date` date DEFAULT NULL,
  `gender` enum('Male','Female','Other') DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(255) DEFAULT NULL,
  `deleted-at` datetime DEFAULT NULL,
  `deleted_by` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

-- Dumping data for table zalon_mountain.customer: ~7 rows (approximately)
/*!40000 ALTER TABLE `customer` DISABLE KEYS */;
INSERT INTO `customer` (`id`, `name`, `password`, `nrc`, `birth_date`, `gender`, `phone`, `email`, `address`, `created_at`, `created_by`, `updated_at`, `updated_by`, `deleted-at`, `deleted_by`) VALUES
	(1, 'Hla', 'ggef', '12/ThaLaNa(N)12345', '2018-07-28', 'Female', '0678976546', 'hla@gmail.com', 'Yangon', '2018-07-28 06:54:30', 'guest', '2018-07-28 06:54:30', NULL, NULL, NULL),
	(2, 'Hla', '12fefefe3456', '12/ThaLaNa(N)12345', '2018-07-28', 'Female', '09689756788', 'hla@gmail.com', NULL, '2018-07-28 06:59:40', 'guest', '2018-07-28 06:59:40', NULL, NULL, NULL),
	(6, 'Hla', '123456', '12/ThaLaNa(N)12345', '2018-07-28', 'Female', '09689756788', 'hla@gmail.com', NULL, '2018-07-28 07:01:52', 'guest', '2018-07-28 07:01:52', NULL, NULL, NULL),
	(7, 'Mya', '123456', '12/BaHa(N)67867', '1991-07-18', 'Female', '09546767', 'mya@gmail.com', NULL, '2018-07-28 07:14:26', 'guest', '2018-07-28 07:14:26', NULL, NULL, NULL),
	(8, 'Aye Chan', 'asd123', 'jfdojfioj', '1994-01-09', 'Female', '0978964566', 'ayechan@gmail.com', 'Thanlyin', '2018-08-08 17:46:43', 'guest', '2018-08-08 17:46:43', NULL, NULL, NULL),
	(9, 'Maria Hmue', 'admin123', '12/ThaLaNa(N)116782', '1992-08-24', 'Female', '097975678567', 'mariahmue@gmail.com', NULL, '2018-08-24 18:05:18', 'guest', '2018-08-24 18:05:18', NULL, NULL, NULL),
	(10, 'Aye Chan', 'ayechan', '12/MaAhNa(N)787868', '1994-08-24', 'Female', '096768787', 'ayechan@gmail.com', NULL, '2018-08-24 19:18:07', 'guest', '2018-08-24 19:18:07', NULL, NULL, NULL);
/*!40000 ALTER TABLE `customer` ENABLE KEYS */;

-- Dumping structure for table zalon_mountain.lang_list
CREATE TABLE IF NOT EXISTS `lang_list` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `abbrev` varchar(20) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `updated_by` varchar(255) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `deleted_by` varchar(255) DEFAULT NULL,
  `special_data` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- Dumping data for table zalon_mountain.lang_list: ~5 rows (approximately)
/*!40000 ALTER TABLE `lang_list` DISABLE KEYS */;
INSERT INTO `lang_list` (`id`, `name`, `abbrev`, `created_at`, `created_by`, `updated_at`, `updated_by`, `deleted_at`, `deleted_by`, `special_data`) VALUES
	(1, 'English', 'en', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(2, 'Chinese', 'zh', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(3, 'Indonesia', 'ind', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(4, 'Malaysia', 'my', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(5, 'Myanmar', 'mm', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
/*!40000 ALTER TABLE `lang_list` ENABLE KEYS */;

-- Dumping structure for table zalon_mountain.lang_presentation
CREATE TABLE IF NOT EXISTS `lang_presentation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `template` varchar(255) DEFAULT NULL,
  `en` varchar(255) DEFAULT NULL,
  `zh` varchar(255) DEFAULT NULL,
  `ind` varchar(255) DEFAULT NULL,
  `vi` varchar(255) DEFAULT NULL,
  `mm` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `updated_by` varchar(255) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `deleted_by` varchar(255) DEFAULT NULL,
  `special_data` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;

-- Dumping data for table zalon_mountain.lang_presentation: ~16 rows (approximately)
/*!40000 ALTER TABLE `lang_presentation` DISABLE KEYS */;
INSERT INTO `lang_presentation` (`id`, `template`, `en`, `zh`, `ind`, `vi`, `mm`, `created_at`, `created_by`, `updated_at`, `updated_by`, `deleted_at`, `deleted_by`, `special_data`) VALUES
	(1, 'login', 'Login', 'Login', 'Login', 'Login', 'Login', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(2, 'number', 'Number', 'Number', 'Number', 'Number', 'Number', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(3, 'search', 'Search', 'Search', 'Search', 'Search', 'Search', '2018-01-03 00:17:42', 'guest', '2018-01-03 00:17:42', NULL, NULL, NULL, NULL),
	(4, 'filter_name', 'Filter Name', 'Filter Name', 'Filter Name', 'Filter Name', 'Filter Name', '2018-01-03 00:17:42', 'guest', '2018-01-03 00:17:42', NULL, NULL, NULL, NULL),
	(5, 'active_filter', 'Active Filter', 'Active Filter', 'Active Filter', 'Active Filter', 'Active Filter', '2018-01-03 00:17:42', 'guest', '2018-01-03 00:17:42', NULL, NULL, NULL, NULL),
	(7, 'from_Date', 'From Date', 'From Date', 'From Date', 'From Date', 'From Date', '2018-01-03 00:17:43', 'guest', '2018-01-03 00:17:43', NULL, NULL, NULL, NULL),
	(8, 'to_date', 'To Date', 'To Date', 'To Date', 'To Date', 'To Date', '2018-01-03 00:17:43', 'guest', '2018-01-03 00:17:43', NULL, NULL, NULL, NULL),
	(9, 'saved_search', 'Saved Search', 'Saved Search', 'Saved Search', 'Saved Search', 'Saved Search', '2018-01-03 00:17:43', 'guest', '2018-01-03 00:17:43', NULL, NULL, NULL, NULL),
	(10, 'sort_by', 'Sort By', 'Sort By', 'Sort By', 'Sort By', 'Sort By', '2018-01-03 00:17:44', 'guest', '2018-01-03 00:17:44', NULL, NULL, NULL, NULL),
	(11, 'sort_dir', 'Sort Dir', 'Sort Dir', 'Sort Dir', 'Sort Dir', 'Sort Dir', '2018-01-03 00:17:44', 'guest', '2018-01-03 00:17:44', NULL, NULL, NULL, NULL),
	(12, 'ascending', 'Ascending', 'Ascending', 'Ascending', 'Ascending', 'Ascending', '2018-01-03 00:17:44', 'guest', '2018-01-03 00:17:44', NULL, NULL, NULL, NULL),
	(13, 'descending', 'Descending', 'Descending', 'Descending', 'Descending', 'Descending', '2018-01-03 00:17:44', 'guest', '2018-01-03 00:17:44', NULL, NULL, NULL, NULL),
	(14, 'action', 'Action', 'Action', 'Action', 'Action', 'Action', '2018-01-03 00:17:44', 'guest', '2018-01-03 00:17:44', NULL, NULL, NULL, NULL),
	(15, 'clear', 'Clear', 'Clear', 'Clear', 'Clear', 'Clear', '2018-01-03 00:17:44', 'guest', '2018-01-03 00:17:44', NULL, NULL, NULL, NULL),
	(16, 'save', 'Save', 'Save', 'Save', 'Save', 'Save', '2018-01-03 00:17:45', 'guest', '2018-01-03 00:17:45', NULL, NULL, NULL, NULL),
	(17, 'delete', 'Delete', 'Delete', 'Delete', 'Delete', 'Delete', '2018-01-03 00:17:45', 'guest', '2018-01-03 00:17:45', NULL, NULL, NULL, NULL);
/*!40000 ALTER TABLE `lang_presentation` ENABLE KEYS */;

-- Dumping structure for table zalon_mountain.room
CREATE TABLE IF NOT EXISTS `room` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `room_no` int(11) DEFAULT NULL,
  `room_type_id` int(11) DEFAULT NULL,
  `status` enum('free','rent') DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(255) DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8;

-- Dumping data for table zalon_mountain.room: ~30 rows (approximately)
/*!40000 ALTER TABLE `room` DISABLE KEYS */;
INSERT INTO `room` (`id`, `room_no`, `room_type_id`, `status`, `created_at`, `created_by`, `updated_at`, `updated_by`, `deleted_at`, `deleted_by`) VALUES
	(1, 101, 1, 'free', NULL, NULL, NULL, NULL, NULL, NULL),
	(2, 102, 1, 'free', NULL, NULL, NULL, NULL, NULL, NULL),
	(3, 103, 1, 'free', NULL, NULL, NULL, NULL, NULL, NULL),
	(4, 104, 1, 'free', NULL, NULL, NULL, NULL, NULL, NULL),
	(5, 105, 1, 'free', NULL, NULL, NULL, NULL, NULL, NULL),
	(6, 106, 2, 'free', NULL, NULL, NULL, NULL, NULL, NULL),
	(7, 107, 2, 'free', NULL, NULL, NULL, NULL, NULL, NULL),
	(8, 108, 3, 'free', NULL, NULL, NULL, NULL, NULL, NULL),
	(9, 201, 1, 'free', NULL, NULL, NULL, NULL, NULL, NULL),
	(10, 202, 1, 'free', NULL, NULL, NULL, NULL, NULL, NULL),
	(11, 203, 1, 'free', NULL, NULL, NULL, NULL, NULL, NULL),
	(12, 204, 2, 'free', NULL, NULL, NULL, NULL, NULL, NULL),
	(13, 205, 2, 'free', NULL, NULL, NULL, NULL, NULL, NULL),
	(14, 206, 2, 'free', NULL, NULL, NULL, NULL, NULL, NULL),
	(15, 207, 3, 'free', NULL, NULL, NULL, NULL, NULL, NULL),
	(16, 208, 3, 'free', NULL, NULL, NULL, NULL, NULL, NULL),
	(17, 301, 1, 'free', NULL, NULL, NULL, NULL, NULL, NULL),
	(18, 302, 1, 'free', NULL, NULL, NULL, NULL, NULL, NULL),
	(19, 208, 2, 'free', NULL, NULL, NULL, NULL, NULL, NULL),
	(20, 303, 2, 'free', NULL, NULL, NULL, NULL, NULL, NULL),
	(21, 304, 2, 'free', NULL, NULL, NULL, NULL, NULL, NULL),
	(22, 305, 2, 'free', NULL, NULL, NULL, NULL, NULL, NULL),
	(23, 306, 3, 'free', NULL, NULL, NULL, NULL, NULL, NULL),
	(24, 307, 3, 'free', NULL, NULL, NULL, NULL, NULL, NULL),
	(25, 308, 3, 'free', NULL, NULL, NULL, NULL, NULL, NULL),
	(26, 401, 2, 'free', NULL, NULL, NULL, NULL, NULL, NULL),
	(27, 402, 3, 'free', NULL, NULL, NULL, NULL, NULL, NULL),
	(28, 403, 3, 'free', NULL, NULL, NULL, NULL, NULL, NULL),
	(29, 409, 3, 'free', NULL, NULL, NULL, NULL, NULL, NULL),
	(30, 410, 3, 'free', NULL, NULL, NULL, NULL, NULL, NULL);
/*!40000 ALTER TABLE `room` ENABLE KEYS */;

-- Dumping structure for table zalon_mountain.room_type
CREATE TABLE IF NOT EXISTS `room_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `no_of_room` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(255) DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- Dumping data for table zalon_mountain.room_type: ~2 rows (approximately)
/*!40000 ALTER TABLE `room_type` DISABLE KEYS */;
INSERT INTO `room_type` (`id`, `name`, `no_of_room`, `created_at`, `created_by`, `updated_at`, `updated_by`, `deleted_at`, `deleted_by`) VALUES
	(1, 'Standard', 10, NULL, NULL, NULL, NULL, NULL, NULL),
	(2, 'Superior', 10, NULL, NULL, NULL, NULL, NULL, NULL),
	(3, 'Delux', 10, NULL, NULL, NULL, NULL, NULL, NULL);
/*!40000 ALTER TABLE `room_type` ENABLE KEYS */;

-- Dumping structure for table zalon_mountain.user
CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `code` varchar(255) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `mobile` varchar(255) DEFAULT NULL,
  `gender` enum('Male','Female') DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `updated_by` varchar(255) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `deleted_by` varchar(255) DEFAULT NULL,
  `special_data` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- Dumping data for table zalon_mountain.user: ~0 rows (approximately)
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` (`id`, `code`, `username`, `password`, `email`, `mobile`, `gender`, `created_at`, `created_by`, `updated_at`, `updated_by`, `deleted_at`, `deleted_by`, `special_data`) VALUES
	(1, '1', 'root', '$2y$10$wlmVNq5ClZBiMk.9ogoVJusYk8R3mjECI6SrBBMKFX1DeexOq4UNu', 'root@root.com', '96369079', 'Female', NULL, NULL, '2017-12-22 17:51:14', NULL, NULL, NULL, NULL);
/*!40000 ALTER TABLE `user` ENABLE KEYS */;

-- Dumping structure for table zalon_mountain.user_access
CREATE TABLE IF NOT EXISTS `user_access` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `acc_admin_menu_id` int(11) DEFAULT NULL,
  `acc_admin_menu_option_id` int(11) DEFAULT NULL,
  `access_type` enum('none','full','custom') DEFAULT NULL,
  `custom_access` text,
  `created_at` timestamp NULL DEFAULT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `updated_by` varchar(255) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `deleted_by` varchar(255) DEFAULT NULL,
  `special_data` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table zalon_mountain.user_access: ~0 rows (approximately)
/*!40000 ALTER TABLE `user_access` DISABLE KEYS */;
/*!40000 ALTER TABLE `user_access` ENABLE KEYS */;

-- Dumping structure for table zalon_mountain.user_session
CREATE TABLE IF NOT EXISTS `user_session` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `token` varchar(255) DEFAULT NULL,
  `ip_address` varchar(255) DEFAULT NULL,
  `login_time` datetime DEFAULT NULL,
  `logout_time` datetime DEFAULT NULL,
  `last_action` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(255) DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=47 DEFAULT CHARSET=utf8;

-- Dumping data for table zalon_mountain.user_session: ~45 rows (approximately)
/*!40000 ALTER TABLE `user_session` DISABLE KEYS */;
INSERT INTO `user_session` (`id`, `user_id`, `token`, `ip_address`, `login_time`, `logout_time`, `last_action`, `created_at`, `created_by`, `updated_at`, `updated_by`, `deleted_at`, `deleted_by`) VALUES
	(1, 1, 'rWZZIzYSiqHI1yNIpXWmaI2mYCrcxjAlmNUiTILl75FwqKgbob', '127.0.0.1', '2018-08-09 16:33:23', NULL, '2018-08-09 16:33:23', '2018-08-09 16:33:25', 'System', '2018-08-09 16:33:25', NULL, NULL, NULL),
	(2, 1, '7vUfyvDgw6lIpDJfdZsG6oSXb9b2Us9p7tigyaIAhQtVmoklJo', '127.0.0.1', '2018-08-09 16:53:42', NULL, '2018-08-09 16:53:42', '2018-08-09 16:53:42', 'System', '2018-08-09 16:53:42', NULL, NULL, NULL),
	(3, 1, 'EthMUVVsCtJh20LacjzdzxmFAJHHMroIcKKWQBJrTZANHchCW3', '127.0.0.1', '2018-08-11 13:11:28', NULL, '2018-08-11 13:20:20', '2018-08-11 13:11:30', 'System', '2018-08-11 13:20:20', 'guest', NULL, NULL),
	(4, 1, 'PO7VHlqxc5T0QBR3CPHg1zPFpYSbktILKx6hrVVuD9th785DNd', '127.0.0.1', '2018-08-11 13:20:23', NULL, '2018-08-11 13:29:51', '2018-08-11 13:20:24', 'System', '2018-08-11 13:29:51', 'guest', NULL, NULL),
	(5, 1, 'r3YGJv8JfSIb2TMj0pfOMQfLcPdhWTadqIcjh29r4uSOPXvAbZ', '127.0.0.1', '2018-08-11 13:20:57', NULL, '2018-08-11 13:20:57', '2018-08-11 13:20:57', 'System', '2018-08-11 13:20:57', NULL, NULL, NULL),
	(6, 1, 'Dn3VWbmyjCqjmJ70M7ZuLqeHdpxbwKiWgaHYeWHZ73fF26TNPl', '127.0.0.1', '2018-08-11 13:22:17', NULL, '2018-08-11 13:22:17', '2018-08-11 13:22:17', 'System', '2018-08-11 13:22:17', NULL, NULL, NULL),
	(7, 1, 'bxBH2dfiTQkv8MmE8qDHEaUm8EpBPojwCxc6hf7Xzw85rGSnbb', '127.0.0.1', '2018-08-11 13:27:05', NULL, '2018-08-11 13:27:05', '2018-08-11 13:27:05', 'System', '2018-08-11 13:27:05', NULL, NULL, NULL),
	(8, 1, 'zgSeVwYGy96QrKQqgJdWFwnsBUoL6bcHYvZGYFcMgPACVUK6aO', '127.0.0.1', '2018-08-11 13:28:16', NULL, '2018-08-11 13:28:16', '2018-08-11 13:28:16', 'System', '2018-08-11 13:28:16', NULL, NULL, NULL),
	(9, 1, 'LrZEjFopTa9SqStjRPA2CfxNACxYrsvF5oAQK34TJGZUdSPzk3', '127.0.0.1', '2018-08-11 13:29:14', NULL, '2018-08-11 13:29:14', '2018-08-11 13:29:14', 'System', '2018-08-11 13:29:14', NULL, NULL, NULL),
	(10, 1, 'I9BkKGoqdepK7I7scBSokkBOCpAHSArcoaaEdpIF2LijD6FN2S', '127.0.0.1', '2018-08-11 13:29:59', NULL, '2018-08-11 14:03:57', '2018-08-11 13:29:59', 'System', '2018-08-11 14:03:57', 'guest', NULL, NULL),
	(11, 1, 'LIzY7pxfcXGm97o5Lm1NhmpuUHNTkqXaAG3b9SQMrO5kbLcfhS', '127.0.0.1', '2018-08-11 14:04:01', NULL, '2018-08-11 14:15:41', '2018-08-11 14:04:03', 'System', '2018-08-11 14:15:41', 'guest', NULL, NULL),
	(12, 1, 'dbnR0915bLzDo7fBsW7Hi2a5QvzorNlkARK0NpHHS62cC1l4Z7', '127.0.0.1', '2018-08-11 14:15:45', NULL, '2018-08-11 14:20:12', '2018-08-11 14:15:45', 'System', '2018-08-11 14:20:12', 'guest', NULL, NULL),
	(13, 1, '74zB4UK0czgxkxxbZfJNfm5FYyY6i2kjsOVQrFdT5iMIh6yORk', '127.0.0.1', '2018-08-11 17:42:06', NULL, '2018-08-11 18:04:16', '2018-08-11 17:42:08', 'System', '2018-08-11 18:04:16', 'guest', NULL, NULL),
	(14, 1, 'OFdljJBfMK3iqTjdNqXMLiddEXiXvjNXacvoflh9x98hTLsTZ5', '127.0.0.1', '2018-08-11 18:04:20', NULL, '2018-08-11 18:06:39', '2018-08-11 18:04:21', 'System', '2018-08-11 18:06:39', 'guest', NULL, NULL),
	(15, 1, 'Qsev2zaBzrvB2SjPtpBbxFMWOoN2amrHmxmSos75xOyuuu2bhI', '127.0.0.1', '2018-08-11 18:06:41', NULL, '2018-08-11 18:07:08', '2018-08-11 18:06:41', 'System', '2018-08-11 18:07:08', 'guest', NULL, NULL),
	(16, 1, 'Otp2Rmj72IT6KA5FmwMGPeuHEMZ0T3MGGwAtFlbOlJkiSruyBK', '127.0.0.1', '2018-08-11 18:07:12', NULL, '2018-08-11 18:10:28', '2018-08-11 18:07:12', 'System', '2018-08-11 18:10:28', 'guest', NULL, NULL),
	(17, 1, 'ufj1bs9jMJKmvlcrqxTjweVz9vJ9iUgQnfwrpZqmpJmt73f5ih', '127.0.0.1', '2018-08-14 15:56:01', NULL, '2018-08-14 16:31:58', '2018-08-14 15:56:02', 'System', '2018-08-14 16:32:00', 'guest', NULL, NULL),
	(18, 1, 'Sg8Hj71HTGYwv0XdBoscXfV2WfpdZg5QG3ChtIk9XCaUSKBOX5', '127.0.0.1', '2018-08-14 16:41:09', NULL, '2018-08-14 16:41:11', '2018-08-14 16:41:10', 'System', '2018-08-14 16:41:11', 'guest', NULL, NULL),
	(19, 1, 'UqMsL9xKoVcYyJ1itFiDFIwqGdGnVvvkAuFCwr5GxjmXx1Ybv6', '127.0.0.1', '2018-08-15 16:31:20', NULL, '2018-08-15 19:36:14', '2018-08-15 16:31:21', 'System', '2018-08-15 19:36:14', 'guest', NULL, NULL),
	(20, 1, 'ool2P4abkp2xejOY6X3SpHAclktoiFW0o9P89MJhcRSY1fb91I', '127.0.0.1', '2018-08-16 14:55:43', NULL, '2018-08-16 15:11:57', '2018-08-16 14:55:45', 'System', '2018-08-16 15:11:57', 'guest', NULL, NULL),
	(21, 1, 'cWSRiTybrg35Gy69VXAFFsZWwKgsN5e03XWuJjpQJ0vgj2DupL', '127.0.0.1', '2018-08-16 16:26:54', NULL, '2018-08-16 16:26:54', '2018-08-16 16:26:55', 'System', '2018-08-16 16:26:55', NULL, NULL, NULL),
	(22, 1, 'RLrTOhwqxmGVGFWLGz1fgdrfgPp4OBqv0tWQl0uOYWJzCdWo7L', '127.0.0.1', '2018-08-16 17:54:00', NULL, '2018-08-16 17:54:00', '2018-08-16 17:54:02', 'System', '2018-08-16 17:54:02', NULL, NULL, NULL),
	(23, 1, 'OL4w7rkxW1ARThBX1fsTnaVMfgTecpp8puuisjjrTiVdXzWx33', '127.0.0.1', '2018-08-17 14:49:53', NULL, '2018-08-17 14:49:58', '2018-08-17 14:49:56', 'System', '2018-08-17 14:49:58', 'guest', NULL, NULL),
	(24, 1, 'd424epXcSFROIeitNChPWasksqeWpBge9C4dbquQMMQOhbXbg7', '127.0.0.1', '2018-08-17 14:49:57', NULL, '2018-08-17 15:05:04', '2018-08-17 14:49:57', 'System', '2018-08-17 15:05:04', 'guest', NULL, NULL),
	(25, 1, 'RLxLe0KpjMFdhb0kS7RLs6IUeRYKv4YypKwLYF67YeQRuvasOL', '127.0.0.1', '2018-08-18 18:11:19', NULL, '2018-08-18 19:15:37', '2018-08-18 18:11:20', 'System', '2018-08-18 19:15:38', 'guest', NULL, NULL),
	(26, 1, '8c5AjLQKMZCSjZxC1eHpEPh6kduJruc49lhEs3Begev6p28kLQ', '127.0.0.1', '2018-08-21 15:12:46', NULL, '2018-08-21 15:24:54', '2018-08-21 15:12:47', 'System', '2018-08-21 15:24:54', 'guest', NULL, NULL),
	(27, 1, '5O9khutvpMwnQSAxD09ITvJB159lSDfNZhIwplWPlu6P35bgRY', '127.0.0.1', '2018-08-21 18:14:20', NULL, '2018-08-21 18:14:55', '2018-08-21 18:14:24', 'System', '2018-08-21 18:14:55', 'guest', NULL, NULL),
	(28, 1, 'ZTFdWRuOhcgl1aOEYovLVDcpc3VSYPiX6qtTD4gEWjDVomCsL7', '127.0.0.1', '2018-08-22 04:13:02', NULL, '2018-08-22 04:28:47', '2018-08-22 04:13:03', 'System', '2018-08-22 04:28:47', 'guest', NULL, NULL),
	(29, 1, '5cebjvthnD8HFlZ8E51jfy5zS8TlLmH14zwLeQ7AdhTTI78WYU', '127.0.0.1', '2018-08-22 04:28:50', NULL, '2018-08-22 04:30:39', '2018-08-22 04:28:51', 'System', '2018-08-22 04:30:39', 'guest', NULL, NULL),
	(30, 1, 'NpdbtZVp36cVW6QitEYSxQqmcGZnIowaE8adLOCyBgC8uTXhHw', '127.0.0.1', '2018-08-22 04:30:41', NULL, '2018-08-22 04:31:18', '2018-08-22 04:30:41', 'System', '2018-08-22 04:31:18', 'guest', NULL, NULL),
	(31, 1, 'BDG4jB2p7hDnO7393my6s3NkN0NR4IQ501oPJvmZvYBEwHL9rM', '127.0.0.1', '2018-08-22 06:31:32', NULL, '2018-08-22 06:37:57', '2018-08-22 06:31:41', 'System', '2018-08-22 06:37:57', 'guest', NULL, NULL),
	(32, 1, 'HRU0OktyEmdPNcT9jEQEQUV8dhDUaIFaj0BGaHZB5uAWpdHOij', '127.0.0.1', '2018-08-22 06:37:58', NULL, '2018-08-22 07:02:41', '2018-08-22 06:37:59', 'System', '2018-08-22 07:02:41', 'guest', NULL, NULL),
	(33, 1, 'mugBnKAgt8PxCj3EVaXyxEiB1qXLmcn15UuNNvgOXOGcqRXahz', '127.0.0.1', '2018-08-22 07:02:51', NULL, '2018-08-22 07:03:51', '2018-08-22 07:02:51', 'System', '2018-08-22 07:03:51', 'guest', NULL, NULL),
	(34, 1, 'WkeYvYYrFuEA7HVINiL52O2hZcnUFB6Bqi99L7QPlwI2GRN5s9', '127.0.0.1', '2018-08-22 07:03:56', NULL, '2018-08-22 07:06:33', '2018-08-22 07:03:56', 'System', '2018-08-22 07:06:33', 'guest', NULL, NULL),
	(35, 1, 'ArRTGVvmSX6CSjq34QKSeCKriWmGOsegTytGXWyGyee78AzSLu', '127.0.0.1', '2018-08-22 07:06:36', NULL, '2018-08-22 07:55:13', '2018-08-22 07:06:37', 'System', '2018-08-22 07:55:13', 'guest', NULL, NULL),
	(36, 1, 'Jm3Lay7cqn7TL1QEUYrSaFgXlYsGtonTgCXUWpaALiUAECYP56', '127.0.0.1', '2018-08-22 11:11:42', NULL, '2018-08-22 11:26:02', '2018-08-22 11:11:43', 'System', '2018-08-22 11:26:02', 'guest', NULL, NULL),
	(37, 1, 'Jia1ebhZTQj7zGMzanlHgENvVxwcJ8mwevoKG6DdTIvC2djfAh', '127.0.0.1', '2018-08-22 11:26:07', NULL, '2018-08-22 13:34:38', '2018-08-22 11:26:08', 'System', '2018-08-22 13:34:38', 'guest', NULL, NULL),
	(38, 1, 'ptmrTZLJGdUgG0htFq3ThxvGQwdXG8VmtlrNjynjZ97RBWyyWx', '127.0.0.1', '2018-08-22 14:17:55', NULL, '2018-08-22 15:04:20', '2018-08-22 14:17:59', 'System', '2018-08-22 15:04:20', 'guest', NULL, NULL),
	(39, 1, '3IV4H5KQqRPYWdsxs8xTSYWZLXVgILZssuqFAikBoVb6Ajf47z', '127.0.0.1', '2018-08-22 15:04:23', NULL, '2018-08-22 15:11:35', '2018-08-22 15:04:24', 'System', '2018-08-22 15:11:35', 'guest', NULL, NULL),
	(40, 1, 'Auvol5do6fimEuBkYoeLedy5KzNl4VbJIEQOWk0gznFG3aeaHj', '127.0.0.1', '2018-08-22 15:11:38', NULL, '2018-08-22 15:12:30', '2018-08-22 15:11:38', 'System', '2018-08-22 15:12:30', 'guest', NULL, NULL),
	(41, 1, 'Ihofziu5WIBPBGr8wtT8kMx40hSOwCOqP9PiVXRpf9XNO86Lzw', '127.0.0.1', '2018-08-22 15:12:32', NULL, '2018-08-22 16:36:38', '2018-08-22 15:12:32', 'System', '2018-08-22 16:36:38', 'guest', NULL, NULL),
	(42, 1, 'Iz3xS9zMsQpqcUKGGCY7ZRF3C2m0MQs4LBCDy7DFwIdZ6yotQc', '127.0.0.1', '2018-08-22 16:36:45', NULL, '2018-08-22 17:09:09', '2018-08-22 16:36:46', 'System', '2018-08-22 17:09:09', 'guest', NULL, NULL),
	(43, 1, '3WFkwuxPSAX7zevrmBhZ6fCSWuY44eDmgxhbjMAu1mOJK5VVbv', '127.0.0.1', '2018-08-22 18:16:42', NULL, '2018-08-22 18:26:39', '2018-08-22 18:16:44', 'System', '2018-08-22 18:26:39', 'guest', NULL, NULL),
	(44, 1, 'WkxrpDKDh3TfjfmvUPYeZGlUikCDM1nFB0d73bXrGYOtKnxLoM', '127.0.0.1', '2018-08-22 18:26:42', NULL, '2018-08-22 18:31:29', '2018-08-22 18:26:43', 'System', '2018-08-22 18:31:29', 'guest', NULL, NULL),
	(45, 1, 'RbxgH7Rxcdvtnf85SArrpo7Uz0AYVvlKwWZCg7fvJ0mqIIamb2', '127.0.0.1', '2018-08-22 18:31:35', NULL, '2018-08-22 19:16:43', '2018-08-22 18:31:36', 'System', '2018-08-22 19:16:43', 'guest', NULL, NULL),
	(46, 1, 'GjHbu6BMDiqYDpqaJYjY2DeXPnHwfYJDMRNM1eLMbiXodjVgos', '127.0.0.1', '2018-08-23 15:31:10', NULL, '2018-08-23 15:57:03', '2018-08-23 15:31:12', 'System', '2018-08-23 15:57:03', 'guest', NULL, NULL);
/*!40000 ALTER TABLE `user_session` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
