import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, App } from 'ionic-angular';
import { BmhotelPage } from '../bmhotel/bmhotel';
import { LoginPage } from '../login/login';

@IonicPage()
@Component({
  selector: 'page-hotels',
  templateUrl: 'hotels.html',
})
export class HotelsPage {
bmhotel:BmhotelPage;
  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    private app : App,) {
 
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad HotelsPage');
  }
  goBmhotelPage() {
    this.navCtrl.push(BmhotelPage);
  }
  goLoginPage() {
    this.app.getRootNav().setRoot('LoginPage');
  }
}
