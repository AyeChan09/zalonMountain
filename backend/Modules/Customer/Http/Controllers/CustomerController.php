<?php

namespace Modules\Customer\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Schema;

use Modules\Customer\Entities\Customer;
use Modules\Booking\Entities\Booking;

class CustomerController extends Controller
{
    private $entity;
    public function __construct(){
        $this->entity = new Customer;
    }
    public function index()
    {
        return view('customer::index');
    }

    // public function register(Request $request){
    //     $return = array();
    //     $input = $request->input();
      
    //     $data = new $this->entity; 
    //     foreach($input as $key=>$value){
    //         if(Schema::hasColumn($data->getTable(), $key)){ 
    //             $data->$key = $value;
    //         }
    //     }
    //     $data->save();
    //     $return['error'] = false;
    //     return $return;
    // }
    
    public function login(Request $request){
        $input = $request->input();
        $data = $this->entity
        ->where("name",$input['name'])
        ->where("password",$input['password'])
        ->first(); 
        
        if(isset($data)){
            $today = date('Y-m-d');
            $booking = Booking::where('start_date', '>', $today)->where('customer_id', $data->id)->first();
            if($booking) {
                $isBooking = 1;
            } else {
                $isBooking = 0;
            }

            $return['data'] = $data;
            $return['isBooking'] = $isBooking;
            $return['error'] = false;
            $return['msg'] = 'success';
        }
        else{
            $return['data'] = $data;
            $return['error'] = true;
            $return['msg'] = 'fail';
        }
        return $return;
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('customer::create');
    }

    public function store(Request $request)
    {
        $input = $request->input();
        $data = $this->entity;
        foreach($input as $key=>$value){
            if(Schema::hasColumn($data->getTable(), $key)){
                $data->$key=$value;
            }
        }
        $data->save();
        $return['data'] = $data;
        $return['err'] = false;
        $return['message'] = 'successful'; 
        return $return;
    }

    public function show(Request $request, $id)
    {
        $data = $this->entity->where('id', $id)->first();
        return $data;
    }

    

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        return view('customer::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request)
    {
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy()
    {
    }
}
