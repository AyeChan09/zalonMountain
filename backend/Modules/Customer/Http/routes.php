<?php

Route::group(['middleware' => [], 'prefix' => 'customer', 'namespace' => 'Modules\Customer\Http\Controllers'], function()
{
    Route::get('/', 'CustomerController@index');
    Route::resource('customer','CustomerController');
    Route::post('login','CustomerController@login');
});
