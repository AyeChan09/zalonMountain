<?php

Route::group(['middleware' => [], 'prefix' => 'booking', 'namespace' => 'Modules\Booking\Http\Controllers'], function()
{
    Route::get('/', 'BookingController@index');
    Route::resource('booking', 'BookingController');
    Route::get('get_data_by_customer/{id}','BookingController@getDataByCustomer');
    Route::get('check_room', 'BookingController@checkRoom');
    Route::get('get_booking_data/{id}', 'BookingController@getBookingData');
    Route::post('set_room', 'BookingController@setRoom');
});
