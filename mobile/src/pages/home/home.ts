import { Component, ViewChild } from '@angular/core';
import { NavController, Events, NavParams, App, IonicPage } from 'ionic-angular';
import { SuperTabs } from 'ionic2-super-tabs';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  pages = [


    { pageName: 'RoutesPage', title: 'History', icon: 'albums', id: 'featTab' },
    
    { pageName: 'HotelsPage', title: 'Hotel', icon: 'home', id: 'subCatTab1' },
    { pageName: 'LoginPage', title: 'Login', icon: 'contact', id: 'subCatTab2' },
    //{ pageName: 'MapPage', title: 'Map', icon: 'map', id: 'subCatTab3' }
    ];
  selectedTab = 0;

  @ViewChild(SuperTabs) superTabs: SuperTabs;

  constructor(
    public navCtrl: NavController,
    public events: Events,
    private app : App,
    public navParams: NavParams) {
      
  }
 
  onTabSelect(ev: any) {
    this.selectedTab = ev.index;
    if(ev.index == 2) {
      this.app.getRootNav().setRoot('LoginPage');
    } else {
      this.selectedTab = ev.index;
    }
  }
}
