import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BmhotelPage } from './bmhotel';

@NgModule({
  declarations: [
    BmhotelPage,
  ],
  imports: [
    IonicPageModule.forChild(BmhotelPage),
  ],
})
export class BmhotelPageModule {}
