import { Pipe, PipeTransform } from '@angular/core';

/**
 * Generated class for the CountdownTimePipe pipe.
 *
 * See https://angular.io/api/core/Pipe for more info on Angular Pipes.
 */
@Pipe({
  name: 'countdownTime',
})
export class CountdownTimePipe implements PipeTransform {
  /**
   * Takes a value and makes it lowercase.
   */
  transform(value: number) : string {
    let diff = value;
    let daysFloat = Math.floor(diff/60/60/24);
    let days = daysFloat.toFixed(0);
    diff =  diff - (Number(days) * 60 * 60 * 24);
    let hoursFloat = Math.floor(diff/60/60);
    let hours = hoursFloat.toFixed(0);
    diff =  diff - (Number(hours) * 60 * 60);
    let minutesFloat = Math.floor(diff/60);
    let minutes = minutesFloat.toFixed(0);
    diff =  diff - (Number(minutes) * 60);
    let seconds = Math.floor(diff).toFixed(0);
    
    
    return days+" days"+" "+hours+":"+minutes+":"+seconds;
  }
}
