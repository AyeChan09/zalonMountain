import { Component, OnInit, Input } from '@angular/core';
import { HttpService } from '../../../shared/http.service';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-customer-check',
  templateUrl: './customer-check.component.html',
  styleUrls: ['./customer-check.component.scss']
})
export class CustomerCheckComponent implements OnInit {
  @Input() id;
  data : any = []; 

  constructor(
    public httpService: HttpService,
    public activeModal: NgbActiveModal
  ) { }

  ngOnInit() {
    this.httpService.get("/customer/customer/"+this.id).subscribe((data) =>{
      let res = data.json();
      this.data = res;
    });
  }

  close() {
    this.activeModal.close();
  }

}
