<?php

namespace App\Http\Middleware;

use Closure;
use Carbon\Carbon;
use Modules\Auth\Entities\UserSession;
use Modules\Auth\Entities\User;

use Illuminate\Http\Response;

class Authentication
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        
        $authToken = $request->input("Authorization");
        $currentTime = Carbon::now();
        if($authToken !== null){
            $authCheck = UserSession::where("token",$authToken)->whereNull("logout_time")->first();
            // if(isset($authCheck->id) && $currentTime->diffInMinutes(Carbon::parse($authCheck->last_action)) <=60){
            if(isset($authCheck->id)){
                $authCheck->last_action = $currentTime;
                $authCheck->session_id = $authCheck->id;
                $authCheck->save();
                
                $user = User::find($authCheck->user_id);
            
                if(isset($user->id)){
                    session(["user_name"=>$user->username,"user_id"=>$user->id,"company_id"=>$authCheck->active_company,"session_id"=>$authCheck->id]);
                }else{
                    session(["user_name"=>"guest"]);
                }
                return $next($request);
            }else{

                return array("msg"=>"invalid_token","status"=>500);
            }
        }else{
            return array("msg"=>"bad_request","status"=>500);            
        }
    }
}
