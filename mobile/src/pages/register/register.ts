import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Platform, Loading, ModalController, ActionSheetController } from 'ionic-angular';
import { ViewController } from 'ionic-angular/navigation/view-controller';
import { FormGroup, FormBuilder, FormArray, Validators } from '@angular/forms';
import { PopoverController } from 'ionic-angular';
import * as moment from 'moment';
import { HttpProvider } from '../../providers/http/http';
import { ConstantProvider } from '../../providers/constant/constant';
import { EmailValidator } from './../../providers/validators/email';

@IonicPage({
  name: 'register'
})
@Component({
  selector: 'page-register',
  templateUrl: 'register.html',
})
export class RegisterPage {
  id: number = 0;
  isEdit: boolean = false;
  data: any;
  class: any = [];
  registerForm: FormGroup;
  gender: any = ['Male', 'Female', 'Other'];
  pwInputType = 'password';
  seen: boolean = true;
  
  loading: Loading;
  base64_photo: string = "";

  temperature_unit: string;
  weight_unit: string;

  has_emergency_contact: boolean;
  parent_arr: any = [];
  parents: FormArray;
  deleted_parents: FormArray;  

  health_record_arr: any = [];
  health_records: FormArray;

  today = moment().format("YYYY-MM-DD");

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public formBuilder: FormBuilder,
    public platform: Platform,
    private viewCtrl: ViewController,
    public modalCtrl: ModalController,
    public popoverCtrl: PopoverController,
    public actionsheetCtrl: ActionSheetController,
    public httpProvider: HttpProvider,
    public constantProvider: ConstantProvider,
  ) {
    // FOR EDIT
    if(navParams.data.id){
      this.id = navParams.data.id;
      this.isEdit = true;
      // this.apiService.get("student/edit/" + this.id).subscribe((data)=>{
      //   let res = data.json();               
      //   this.data = res.data;
      //   this.base64_photo = res.data.photo;
      //   this.registerForm.patchValue(res.data);
      //   let parents: any[] = res.data.parents; 
      //   parents.forEach(values => {
      //     this.doAddParent(values);
      //   });
      // });
    }
    
    this.buildForm();
  }

  ionViewWillEnter() {
      this.viewCtrl.showBackButton(this.isEdit);
  }

  viewDashboard() {
    this.navCtrl.setRoot("home");
  }

  buildForm() {
    if(!this.isEdit){
      this.registerForm = this.formBuilder.group({
        name: ['', Validators.required],
        password: ['', Validators.compose([Validators.required, Validators.minLength(6)])],
        nrc: ['', Validators.compose([Validators.required,Validators.pattern(/^([\d]{1,2})\/([\w]{3}|[\w]{6})\(?::NAING|N\)([\d]{6})$/)])],
        birth_date: ['', Validators.required],
        gender: [''],
        phone: ['', Validators.required],
        email: ['', Validators.compose([Validators.required, EmailValidator.isValid])],
        address: ['',],
      });
    }else{
      this.registerForm = this.formBuilder.group({
        name: ['', Validators.required],
        password: ['', Validators.compose([Validators.required, Validators.minLength(6)])],
        nrc: ['', Validators.compose([Validators.required,Validators.pattern(/^([\d]{1,2})\/([\w]{3}|[\w]{6})\(?::NAING|N\)([\d]{6})$/)])],
        birth_date: ['', Validators.required],
        gender: [''],
        phone: ['', Validators.required],
        email: ['', Validators.compose([Validators.required, EmailValidator.isValid])],
        address: ['',],
      });
    }
  }

  onSubmit({value, valid}) {
    if(valid){
      this.loading = this.constantProvider.loading();

      let parameter : any = {};
      parameter = value;
      console.log('parameter=>', parameter);
      if(!this.isEdit){        
        let rt : any = this.httpProvider.post("customer/customer",parameter);
        rt.subscribe((data) =>{
          let res = data.json();
          this.loading.dismiss().then(() => {
            this.constantProvider.toast("Successfully registered!");
            localStorage.setItem("id",res.data.id);
            this.navCtrl.push("booking");
          })          
        });
      }else{
        let rt : any = this.httpProvider.put("customer/customer/" + this.id ,parameter);
        rt.subscribe((data) =>{
          this.loading.dismiss();
          this.constantProvider.toast("Register updated!");

          // this.navCtrl.push("booking");
        });
      }
    }
  }

  seePassword(){
    this.seen = !this.seen;
    if(this.seen){
      this.pwInputType = "password"
    } else {
      this.pwInputType = "text"
    }
  }
}
