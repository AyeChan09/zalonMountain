import { Routes, RouterModule } from '@angular/router';
import { ModuleWithProviders } from '@angular/core';

import { DashboardComponent } from './dashboard/dashboard.component';

import { SynapseComponent } from './synapse.component';
import { LoginComponent } from "./authentication/login/login.component";

export const routes: Routes = [
    { path: 'login', component:LoginComponent, data: { breadcrumb: 'Login' } },
    {
        path: '', 
        component: SynapseComponent,
        children:[
            { path: 'dashboard',component:DashboardComponent},            
            { path: 'checking', loadChildren: 'app/synapse/checking/checking.module#CheckingModule', data: { breadcrumb: 'Checking'}},
        ]
    },
    
    
];

export const routing: ModuleWithProviders = RouterModule.forChild(routes);