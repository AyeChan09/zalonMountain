<?php

namespace Modules\Customer\Entities;

use App\ARmodel as Model;

class Customer extends Model
{
    protected $fillable = [];
    protected $table = "customer";
}
