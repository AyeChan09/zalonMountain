<?php

namespace Modules\Room\Entities;

use App\ARmodel as Model;

class RoomType extends Model
{
    protected $fillable = [];
    protected $table = "room_type";
}
