import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Loading } from 'ionic-angular';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { ConstantProvider } from '../../providers/constant/constant';
import { HttpProvider } from '../../providers/http/http';

@IonicPage({
  name: 'CashPage'
})
@Component({
  selector: 'page-cash',
  templateUrl: 'cash.html',
})
export class CashPage {
  cashForm: FormGroup;
  loading: Loading;
  data: any = {};
  total: number;
  receiver: string;

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public formBuilder: FormBuilder,
    public constantProvider: ConstantProvider,
    public httpProvider: HttpProvider) {
      this.data = this.navParams.data;
      this.receiver = '09799342456';
      this.httpProvider.get("booking/booking/"+ this.data.booking).subscribe((data) => {
        let res = data.json();
        this.total = res.data.total_amount;
      });
      this.buildForm();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CashPage');
  }

  buildForm() {
  this.cashForm = this.formBuilder.group({
      receiver_phone: [''],
      phone_no: ['', Validators.required],
      password: ['', Validators.compose([Validators.required, Validators.minLength(6)])],
      total_amount: [this.total],
      trx_id: ['', Validators.compose([Validators.required, Validators.minLength(9)])]
    });
  }

  onSubmit({value, valid}) {
    if(valid){
      this.loading = this.constantProvider.loading();

      let parameter : any = {};
      parameter = value;
      parameter['booking_id'] = this.data.booking;
      parameter['customer_id'] = this.data.customer;
      let rt : any = this.httpProvider.post("cash/cash",parameter);
      rt.subscribe((data) =>{
        let res = data.json();
        this.loading.dismiss().then(() => {
          this.constantProvider.toast("Successfully!  ");
          this.navCtrl.push("confirm");
        })          
      });      
    }
  }
}
