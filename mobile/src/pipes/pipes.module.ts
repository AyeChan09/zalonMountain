import { NgModule } from '@angular/core';
import { CountdownTimePipe } from './countdown-time/countdown-time';
import { TruncatePipe } from './truncate/truncate';
@NgModule({
	declarations: [
		CountdownTimePipe,
		TruncatePipe
	],
	imports: [],
	exports: [
		CountdownTimePipe,
		TruncatePipe
	]
})
export class PipesModule {}
