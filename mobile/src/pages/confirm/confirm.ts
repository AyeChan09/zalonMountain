import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, Loading, Content } from 'ionic-angular';
import { ConstantProvider } from '../../providers/constant/constant';
import { HttpProvider } from '../../providers/http/http';
import { Validators, FormGroup, FormBuilder } from '@angular/forms';
import * as moment from 'moment';
import { CashPage } from '../cash/cash';
import { HomePage } from '../home/home';

@IonicPage({
  name: 'confirm'
})
@Component({
  selector: 'page-confirm',
  templateUrl: 'confirm.html',
})
export class ConfirmPage {
  confirmForm: FormGroup;
  loading: Loading;
  isWaiting: boolean = true;
  isConfirm: boolean = false;
  isWave: boolean;
  waveWrong: boolean = false;
  id: number;
  data: any = {};
  isModify: boolean = false;
  @ViewChild(Content) content:Content;
  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public constantProvider: ConstantProvider,
    public httpProvider: HttpProvider,
    public formBuilder: FormBuilder,
  ) {
      this.id = parseInt(localStorage.getItem('id'));
      this.httpProvider.get("booking/get_data_by_customer/"+ this.id).subscribe((data) => {
        let res = data.json();
          if(!res.error) {
            this.data = res.data;
            if(res.cash) {
              this.isWave = true;
              if(res.cash.status == 'wrong')
                this.waveWrong = true;
            } else {
              this.isWave = false;
            }
            let confirm = this.data.status;
            if(confirm != 'waiting')
              this.isWaiting = false;
            if(confirm == 'approve')
              this.isConfirm = true;
            let lastSeven = moment().add(7, 'days').calendar();
            let lastSevenDay = moment(lastSeven).format("YYYY-MM-DD");
            if(lastSevenDay < this.data.start_date) {
              this.isModify = true;
            }
            console.log('isWave=>', this.isWave);
            console.log('waveWrong=>', this.waveWrong);
            console.log('isConfirm=>', this.isConfirm);
            console.log('isModify=>', this.isModify);
          }
      });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ConfirmPage');
  }

  editBooking() {
    this.navCtrl.push('booking', this.data.id);
  }

  refreshBrn() {
    this.navCtrl.setRoot(this.navCtrl.getActive().component);
  }

  sendMoney() {
    let param: any = {};
    param['booking'] = this.data.id;
    param['customer'] = this.data.customer_id;
    this.navCtrl.push('CashPage', param);
  }

  viewDashboard() {
    this.navCtrl.setRoot(HomePage);
  }

  doLogout() {
    this.navCtrl.setRoot(HomePage);
  }
}
