import { Injectable } from '@angular/core';
import { AlertController, ToastController, LoadingController } from 'ionic-angular';
import 'rxjs/add/operator/map';
//import { Storage } from '@ionic/storage';

@Injectable()
export class ConstantProvider {

  public serverPath = "http://localhost:8000/";
  // public serverPath='http://192.168.1.15/ionic_project/marketingApp/marketingApp_laravel/public/';
  // public serverPath = 'http://arzaphosting2.biz/prototype/marketing/marketingApp_laravel/public/';
  public apiPath = this.serverPath;

  constructor(
    public alertCtrl: AlertController,
    public toastCtrl: ToastController,
    public loadCtrl: LoadingController
  ) {
  }

  public getServerPath() {
    return this.serverPath;
  }

  public getApiPath() {
    return this.apiPath;
  }

  public clearToken() {
    localStorage.removeItem("token");
  }
  public setToken(token) {
    localStorage.setItem("token", token);
  }
  public getToken() {
    return localStorage.getItem("token");
  }

  basicAlert(title, msg) {
    let alert = this.alertCtrl.create({
      title: title,
      subTitle: msg,
      buttons: ['OK']
    });
    alert.present();
  }

  confirmAlert(title, msg, text1, text2) {
    return new Promise((resolve, reject) => {
      let alert = this.alertCtrl.create({
        title: title,
        message: msg,
        buttons: [
          {
            text: text1,
            handler: () => {
              resolve(true);
            }
          },
          {
            text: text2,
            role: 'cancel',
            handler: () => {
              resolve(false);
            }
          }
        ]
      });
      alert.present();
    });
  }

  radioAlert(title: string, array: any) {
    return new Promise((resolve, reject) => {
      let alert = this.alertCtrl.create();
      alert.setTitle(title);
      array.forEach((item, index) => {
        alert.addInput({
          type: 'radio',
          label: item.name,
          value: item.value,
          checked: item.status
        });
      });

      alert.addButton({
        text: 'Cancel',
        role: 'cancel',
        handler: () => {
          resolve('cancel');
        }
      });
      alert.addButton({
        text: 'Ok',
        handler: (data: any) => {
          resolve(data);
        }
      });

      alert.present();
    })
  }

  toast(msg) {
    let toast = this.toastCtrl.create({
      message: msg,
      duration: 2500,
      position: 'top'
    });
    toast.present();
  }

  loading() {
    let load = this.loadCtrl.create({
      spinner: 'hide',
      content: `<img src="assets/icon/loading.gif"/>`,
      dismissOnPageChange: false
    });
    load.present();
    return load;
  }

}
