import { Injectable } from '@angular/core';
import { Http, HttpModule, Headers, RequestOptions, RequestMethod, Response, URLSearchParams} from '@angular/http';
import { LocalStorageService } from "ngx-webstorage";
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/delay';
import { RouterModule, Routes, Router } from '@angular/router';

import { APPCONFIG } from '../config';
@Injectable()
export class HttpService {
  public AppConfig: any;
  public token : string = "";
  constructor(
    private http:Http,
   private storage:LocalStorageService,
   private router : Router) {
    this.AppConfig = APPCONFIG;
    this.checkToken(); 
  }
  
  get(url, parameter = {}, headers = {}){
      parameter['Authorization'] = localStorage.getItem('ibeautyUserToken');  
    if(headers['Content-Type']!=="undefined"){
      headers['Content-Type'] = "application/json"
    }
    let httpHeader = new Headers(headers);
    let options = new RequestOptions({ headers : httpHeader, params:parameter  });
    return this.http.get(this.AppConfig.baseUrl+url, options);
    
  }
  post(url, parameter = {}, headers = {}){
    parameter['Authorization'] = localStorage.getItem('ibeautyUserToken');
    if(headers['Content-Type']!=="undefined"){
      headers['Content-Type'] = "application/json"
    }
    let httpHeader = new Headers(headers);
    let options = new RequestOptions({ headers : httpHeader});

    return this.http.post(this.AppConfig.baseUrl+url, parameter, options);
     
  
  }
  put(url, parameter = {}, headers = {}){
    parameter['Authorization'] = localStorage.getItem('ibeautyUserToken');
    if(headers['Content-Type']!=="undefined"){
      headers['Content-Type'] = "application/json"
    }
    let httpHeader = new Headers(headers);
    let options = new RequestOptions({ headers : httpHeader });

    return this.http.put(this.AppConfig.baseUrl+url, parameter, options);
      
  
  }
  delete(url, parameter = {}, headers = {}){
    parameter['Authorization'] = localStorage.getItem('ibeautyUserToken');    
    if(headers['Content-Type']!=="undefined"){
      headers['Content-Type'] = "application/json"
    }
    let httpHeader = new Headers(headers);
    let options = new RequestOptions({ headers : httpHeader });
    // url = url + "?Authorization="+localStorage.getItem('token');
    url = url + "?Authorization="+localStorage.getItem('ibeautyUserToken');
    return this.http.delete(this.AppConfig.baseUrl + url, options);
  }
  newPage(url, parameter = {}){
      parameter['Authorization'] = localStorage.getItem('ibeautyUserToken');  
      let params = new URLSearchParams();
      parameter['search'] = JSON.stringify(parameter['search']);
      parameter['order'] = JSON.stringify(parameter['order']);

      for(let key in parameter){
        params.set(key, parameter[key]);
      }
      
      let target = this.AppConfig.baseUrl+url;
      var tabWindowId = window.open(target+"?"+params.toString(), '_blank');
  }
  checkToken(){
    let headers : any =  {"Content-Type": "application/json"};
    let httpHeader = new Headers(headers);    
    let options = new RequestOptions({ headers : httpHeader });
    
    this.http.post(this.AppConfig.baseUrl+"/auth/session_check", {"Authorization" : localStorage.getItem('ibeautyUserToken')}, options).subscribe((res)=>{
      let ret = res.json();     
      if(ret.error){
        this.router.navigate(["/synapse/login"]);
      }
    });
  }  
}
