<?php

Route::group(['middleware' => [], 'prefix' => 'generic', 'namespace' => 'Modules\Generic\Http\Controllers'], function()
{
    Route::resource('saved_filter', 'SavedFilterController');
    Route::post("call_helper","HelperController@call");
    Route::post("get_access","AccessController@get_list_access");
    Route::post("get_header","AccessController@get_list_header");
    Route::get('get_translate', 'LanguageController@get_translate');
    Route::get('save_translate', 'LanguageController@save_translate');
});
Route::group(['middleware' => [], 'prefix' => 'generic', 'namespace' => 'Modules\Generic\Http\Controllers'], function()
{
    Route::get('get_translate', 'LanguageController@get_translate');
    Route::get('save_translate', 'LanguageController@save_translate');
});
