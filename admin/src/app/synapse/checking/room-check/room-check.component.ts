import { Component, OnInit, Input } from '@angular/core';
import { Location } from '@angular/common';
import * as moment from 'moment';
import { HttpService } from '../../../shared/http.service';
import { ActivatedRoute } from '@angular/router';
import { NgbActiveModal, NgbDateParserFormatter } from '@ng-bootstrap/ng-bootstrap';
import { TranslateService } from '@ngx-translate/core';
import swal from 'sweetalert2';

@Component({
  selector: 'app-room-check',
  templateUrl: './room-check.component.html',
  styleUrls: ['./room-check.component.scss']
})
export class RoomCheckComponent implements OnInit {
  @Input() customer_id;
  public loading = false;
  private sub: any;
  id : number = 0;  
  start_date: any;
  end_date: any;
  room_type: number;
  booking_id: number;
  data: any = [];
  customer_data: any = {};
  roomTypes: any = [];
  isData: boolean = false;
  no_of_room: number;
  user_id: number;

  currentFunction: string = "room";
  tableHeader: any = [];

  constructor(
    public httpService: HttpService,
    private location : Location,
    private activatedRoute : ActivatedRoute,
    public ngbActiveModal: NgbActiveModal,
    public translate: TranslateService,
    private parserFormatter: NgbDateParserFormatter
  ) { 
    this.user_id = parseInt(localStorage.getItem('user'));
    this.httpService.post("/room/call_helper", {function: 'room_type'}).subscribe((data) =>{
      this.roomTypes = data.json();
    });
    this.httpService.post("/generic/get_header", { list_name: this.currentFunction }).subscribe((data) => {
      let res = data.json();
      this.tableHeader = res.data;
    });
  }

  ngOnInit() {
    this.httpService.get("/booking/get_booking_data/"+ this.customer_id).subscribe((data) =>{
      let res = data.json();

      this.customer_data = res;
      this.convertObject(this.customer_data.start_date, this.customer_data.end_date)
      this.room_type = this.customer_data.room_type;
      this.booking_id = this.customer_data.id;
      this.no_of_room = this.customer_data.room_no;
    });
  }

  // getCustomerInfo(customerId) {
  //   this.httpService.get("/customer/get_booking_data", customerId).subscribe((data) =>{
  //     let res = data.json();

  //     this.customer_data = res;
  //     console.log('booking_data=>', this.customer_data);
  //   });
  // }

  getData() {
    let parameter: any = {};
    let start = this.parserFormatter.format(this.start_date);
    parameter.start_date = moment(start).format("YYYY-MM-DD");
    let end = this.parserFormatter.format(this.end_date);
    parameter.end_date = moment(end).format("YYYY-MM-DD");
    parameter.room_type = this.room_type;
    this.isData = false;
    this.httpService.get("/booking/check_room", parameter).subscribe((data) =>{
      let res = data.json();

      this.data = res;
      if(this.data.length > 0) {
        this.isData = true;
      }
    });
  }

  convertObject(start_date, end_date){
    let d1 = start_date.split('-');
    this.start_date = {
      day: parseInt(d1[2]),
      month: parseInt(d1[1]),
      year: parseInt(d1[0])
    };
    let d2 = end_date.split('-');
    this.end_date = {
      day: parseInt(d2[2]),
      month: parseInt(d2[1]),
      year: parseInt(d2[0])
    };
  }

  giveRoomToCustomer(roomId) {
    let main = this;
    let title: any = this.translate.get("Notice!");
    let text: any = this.translate.get("Do you want to give this room?");
    swal({
      title: title.value,
      text: text.value,
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#0CC27E',
      cancelButtonColor: '#FF586B',
      confirmButtonText: 'Confirm',
      cancelButtonText: "Cancel"
    }).then(function (isConfirm) {
      if (isConfirm) {
        main.setBooking(roomId);
      } 
    },function(dismiss) {
      swal.close();
    });
    // swal( "Successful!", "Student created.", "success");
  }

  setBooking(roomId) {
    let main = this;
    this.loading = true;
    let parameter: any = {};
    let start_date = this.parserFormatter.format(this.start_date);
    let end_date = this.parserFormatter.format(this.end_date);
    parameter['start_date'] = moment(start_date).format("YYYY-MM-DD");
    parameter['end_date'] = moment(end_date).format("YYYY-MM-DD");
    parameter['customer_id'] = this.customer_id;
    parameter['room_id'] = roomId;
    parameter['room_type_id'] = this.room_type;
    parameter['booking_id'] = this.booking_id;
    parameter['user_id'] = this.user_id;
    let rt : any = this.httpService.post("/booking/set_room", parameter);
      rt.subscribe((data) =>{
        this.loading = false;
        let res = data.json();
        if(!res.error){
          let title: any = this.translate.get("Success!");
          let text: any = this.translate.get("Room reserved.");
          swal({
            title: title.value,
            text: text.value,
            type: 'success',
            confirmButtonColor: '#0CC27E',
            confirmButtonText: 'Confirm',
          }).then(function (isConfirm) {
            main.getData();
          });
        }
    });
    // this.ngbActiveModal.close('success');
  }

  doCancel() {
    this.ngbActiveModal.close(null);
  }

  doBack() {
    this.ngbActiveModal.close('success');
  }
}
