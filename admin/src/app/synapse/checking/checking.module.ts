import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from "../../shared/shared.module";

import { BookingCheckComponent } from './booking-check/booking-check.component';
import { CustomerCheckComponent } from './customer-check/customer-check.component';
import { RoomCheckComponent } from './room-check/room-check.component';
import { CashCheckComponent } from './cash-check/cash-check.component';

export const routes = [
  { path: 'booking', component: BookingCheckComponent },
  { path: 'room', component: RoomCheckComponent },
  { path: 'cash', component: CashCheckComponent },
];


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
    RouterModule.forChild(routes)
  ],
  declarations: [BookingCheckComponent, CustomerCheckComponent, RoomCheckComponent, CashCheckComponent],
  entryComponents: [CustomerCheckComponent]
})
export class CheckingModule { }
