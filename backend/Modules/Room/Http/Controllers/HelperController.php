<?php

namespace Modules\Room\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;

class HelperController extends Controller
{
    public function call(Request $request){
        $function = $request->input("function");
        return $this->$function($request);
    }

    public function room_type(Request $request){
        $return = DB::table("room_type")->get();
        return $return;
    }
}