<?php

namespace Modules\Room\Entities;

use App\ARmodel as Model;

class Room extends Model
{
    protected $fillable = [];
    protected $table = "room";
}
