<?php

Route::group(['middleware' => [], 'prefix' => 'cash', 'namespace' => 'Modules\Cash\Http\Controllers'], function()
{
    Route::get('/', 'CashController@index');
    Route::resource('cash', 'CashController');
    Route::post('wrong_data/{id}', 'CashController@wrongData');
});
