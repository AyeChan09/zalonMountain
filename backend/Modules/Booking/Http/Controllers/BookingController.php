<?php

namespace Modules\Booking\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

use Modules\Booking\Entities\Booking;
use Modules\Booking\Entities\BookingRoom;
use Modules\Room\Entities\Room;
use Modules\RoomType\Entities\RoomType;
use Modules\Cash\Entities\Cash;

class BookingController extends Controller
{
    private $entity;
    public function __construct(){
        $this->entity = new Booking;
    }
    public function index(Request $request)
    {
        $input=$request->input();
        $page=$input['page'];
        $limit=$input['limit'];
        $data = $this->entity::select('booking.*', 'customer.name as customer_name', 'room_type.name as room_type')
                ->where('status', 'waiting')
                ->leftJoin('customer', 'booking.customer_id', 'customer.id')
                ->leftJoin('room_type', 'booking.room_type', 'room_type.id')
                ->offset(($page-1)*$limit)->limit($limit); 
        
        $totalData = $data->count("booking.id");
        $data=$data->get();
        $return['total'] = $totalData;
        $return['data'] = $data;
        return $return;
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('booking::create');
    }

    public function store(Request $request)
    {
        $input = $request->input();
        $data = $this->entity;
        foreach($input as $key=>$value){
            if(Schema::hasColumn($data->getTable(), $key)){
                $data->$key=$value;
            }
        }
        $data->status = 'waiting';
        $data->save();
        if(isset($data)){
            $return['data'] = $data;
            $return['error'] = false;
            $return['msg'] = 'success';
        }
        else{
            $return['data'] = $data;
            $return['error'] = true;
            $return['msg'] = 'fail';
        }
        return $return;
    }

    public function setRoom(Request $request)
    {
        $input = $request->input();
        $start_date = $input['start_date'];
        $end_date = $input['end_date'];
        $room = $input['room_id'];
        $room_type = $input['room_type_id'];
        $user_id = $input['user_id'];
        
        //Extract Room Price
        $price = Room::where('id', $room)->where('room_type_id', $room_type)->first()->price;

        $date = DB::select("
        select date, day_name from
            (select date, DAYNAME(date) as day_name from
            (select adddate('1970-01-01',t4.i*10000 + t3.i*1000 + t2.i*100 + t1.i*10 + t0.i) date from
                    (select 0 i union select 1 union select 2 union select 3 union select 4 union select 5 union select 6 union select 7 union select 8 union select 9) t0,
                    (select 0 i union select 1 union select 2 union select 3 union select 4 union select 5 union select 6 union select 7 union select 8 union select 9) t1,
                    (select 0 i union select 1 union select 2 union select 3 union select 4 union select 5 union select 6 union select 7 union select 8 union select 9) t2,
                    (select 0 i union select 1 union select 2 union select 3 union select 4 union select 5 union select 6 union select 7 union select 8 union select 9) t3,
                    (select 0 i union select 1 union select 2 union select 3 union select 4 union select 5 union select 6 union select 7 union select 8 union select 9) t4) as date_range
            where date between '" .$start_date. "' and '" .$end_date. "' ) as day_name
       ");
        
        // $data = new $this->entity;                
        // $temp = $data->get();
        $booking = $this->entity->where('id', $input['booking_id'])->first();
        for($i=0; $i<count($date); $i++){
            $data = new BookingRoom;      
            $data['booking_id'] = $input['booking_id'];
            $data['date'] = $date[$i]->date;
            $data['room_id'] = $room;
            $data['room_type_id'] = $room_type;
            $data->save();

            //Add room price to booking
            $booking->total_amount += $price;
            $booking->save();
        }
        $dataForBooking = BookingRoom::where('booking_id', $input['booking_id'])->get();
        if($dataForBooking) {
            $start_date_booking = $dataForBooking[0]->date;
            $end_date_booking = $dataForBooking[0]->date;
            if(count($dataForBooking) > 1) {
                for($i=1; $i<count($dataForBooking); $i++) {
                    if($start_date_booking > $dataForBooking[$i]->date) 
                        $start_date_booking = $dataForBooking[$i]->date;
                    if($end_date_booking < $dataForBooking[$i]->date)
                        $end_date_booking = $dataForBooking[$i]->date;
                }                
            }
        }        
        $booking = $this->entity->where('id', $input['booking_id'])->update(['status'=>'pending', 'accept_person_id'=>$user_id, 'start_date'=>$start_date_booking, 'end_date'=>$end_date_booking]);
        $return['data'] = $booking;
        $return['error'] = false;
        return $return;
    }

    public function checkRoom(Request $request)
    {
        $input = $request->input();
        $startDate = $input['start_date'];
        $endDate = $input['end_date'];
        $data = DB::select("select room.id, room.room_no, room_type.name as room_type, room.status from room
        left join room_type on room.room_type_id = room_type.id
        where room.room_type_id = " .$input['room_type']. " and room.id Not in (
        select room_id 
        from booking_room
        where date >= '".$startDate."' AND date <= '".$endDate."') ");
        // $data = DB::table('room')->leftJoin('room_type', 'room.room_type_id', 'room_type.id')
        //         ->select('room.id', 'room.room_no', 'room_type.name')
        //         ->where('room.room_type_id', $input['room_type'])
        //         ->whereNotIn('room.id', function($query) use ($startDate) {
        //             $query->select('room_id')
        //             ->whereIn('date', '>=', $startDate)
        //             ->where('date', '<=', $endDate)
        //             ->from('booking_room');
        //         });
        return $data;
    }

    public function show(Request $request, $id)
    {
        $return = array();
        $data = $this->entity
                ->select('booking.*', 'customer.name as customer_name')
                ->where('booking.id', $id)
                ->leftJoin('customer', 'booking.customer_id', 'customer.id')
                ->first();
        if(isset($data)){
            $return['data'] = $data;
            $return['error'] = false;
            $return['msg'] = 'success';
        }
        else{
            $return['data'] = $data;
            $return['error'] = true;
            $return['msg'] = 'fail';
        }            
        return $return;
    }

    public function getDataByCustomer(Request $request, $id)
    {
        $return = array();
        $today = date("Y-m-d");
        $data = $this->entity
                ->select('booking.*', 'customer.name as customer_name', DB::raw("date(booking.created_at)"))
                ->where('booking.customer_id', $id)
                ->where(DB::raw("date(booking.start_date)"), '>=', $today)
                ->leftJoin('customer', 'booking.customer_id', 'customer.id')
                ->orderBy('booking.id', 'desc')
                ->first();

        if(isset($data)){
            $return['data'] = $data;
            $return['cash'] = Cash::where('booking_id', $data->id)->orderBy('booking_id', 'desc')->first();
            $return['error'] = false;
            $return['msg'] = 'success';
        }
        else{
            $return['data'] = $data;
            $return['error'] = true;
            $return['msg'] = 'fail';
        }            
        return $return;
    }

    public function getBookingData(Request $request, $id)
    {
        $input = $request->input();
        $data = $this->entity::where('customer_id', $id)->where('status', 'waiting')->orderBy('created_at', 'desc')->first();
        return $data;
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        return view('booking::edit');
    }

    public function update(Request $request, $id)
    {
        $input = $request->input();
        $data = $this->entity->find($id);
        foreach($input as $key=>$value){           
            if(Schema::hasColumn($data->getTable(), $key)){ 
                $data->$key = $value;
            }
        }
        $data->status = 'pending';
        $data->save();
        return $data;   
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy()
    {
    }
}
