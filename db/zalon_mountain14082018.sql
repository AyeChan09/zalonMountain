-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.1.32-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win32
-- HeidiSQL Version:             9.5.0.5196
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for zalon_mountain
CREATE DATABASE IF NOT EXISTS `zalon_mountain` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `zalon_mountain`;

-- Dumping structure for table zalon_mountain.acc_admin_menu_item
CREATE TABLE IF NOT EXISTS `acc_admin_menu_item` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL DEFAULT '',
  `is_show` tinyint(4) DEFAULT NULL,
  `path` varchar(255) NOT NULL DEFAULT '',
  `external_href` varchar(255) DEFAULT NULL,
  `external_href_target` varchar(255) DEFAULT NULL,
  `icon` varchar(255) NOT NULL DEFAULT '',
  `class` varchar(255) NOT NULL DEFAULT '',
  `order_id` int(11) DEFAULT NULL,
  `hasChild` tinyint(1) DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `updated_by` varchar(255) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `deleted_by` varchar(255) DEFAULT NULL,
  `special_data` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- Dumping data for table zalon_mountain.acc_admin_menu_item: ~5 rows (approximately)
/*!40000 ALTER TABLE `acc_admin_menu_item` DISABLE KEYS */;
INSERT INTO `acc_admin_menu_item` (`id`, `title`, `is_show`, `path`, `external_href`, `external_href_target`, `icon`, `class`, `order_id`, `hasChild`, `parent_id`, `created_at`, `created_by`, `updated_at`, `updated_by`, `deleted_at`, `deleted_by`, `special_data`) VALUES
	(1, 'Booking Check', 1, '/synapse/checking/booking', NULL, NULL, 'fa fa-bookmark', '', 1, 0, 0, '2018-08-09 23:11:02', NULL, NULL, NULL, NULL, NULL, NULL),
	(2, 'Room Check', 1, '/synapse/checking/room', NULL, NULL, 'fa fa-address-book', '', 2, 0, 0, '2018-08-09 23:11:02', NULL, NULL, NULL, NULL, NULL, NULL),
	(3, 'Room', 1, '', NULL, NULL, '', '', 3, 1, 0, '2018-08-09 23:11:02', NULL, NULL, NULL, NULL, NULL, NULL),
	(4, 'Room', 1, '/synapse/room/room_list', NULL, NULL, 'fa fa-door-closed', '', 1, 0, 3, '2018-08-09 23:11:02', NULL, NULL, NULL, NULL, NULL, NULL),
	(5, 'Room Type', 1, '/synapse/room/room_type_list', NULL, NULL, 'fa fa-door-open', '', 2, 0, 3, '2018-08-09 23:11:02', NULL, NULL, NULL, NULL, NULL, NULL);
/*!40000 ALTER TABLE `acc_admin_menu_item` ENABLE KEYS */;

-- Dumping structure for table zalon_mountain.acc_admin_menu_item_option
CREATE TABLE IF NOT EXISTS `acc_admin_menu_item_option` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `acc_admin_menu_item_id` int(11) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `updated_by` varchar(255) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `deleted_by` varchar(255) DEFAULT NULL,
  `special_data` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

-- Dumping data for table zalon_mountain.acc_admin_menu_item_option: ~12 rows (approximately)
/*!40000 ALTER TABLE `acc_admin_menu_item_option` DISABLE KEYS */;
INSERT INTO `acc_admin_menu_item_option` (`id`, `acc_admin_menu_item_id`, `name`, `created_at`, `created_by`, `updated_at`, `updated_by`, `deleted_at`, `deleted_by`, `special_data`) VALUES
	(1, 1, 'allow_add', '2018-08-09 23:14:09', NULL, NULL, NULL, NULL, NULL, NULL),
	(2, 1, 'allow_edit', '2018-08-09 23:14:09', NULL, NULL, NULL, NULL, NULL, NULL),
	(3, 1, 'allow_delete', '2018-08-09 23:14:09', NULL, NULL, NULL, NULL, NULL, NULL),
	(4, 2, 'allow_add', '2018-08-09 23:14:09', NULL, NULL, NULL, NULL, NULL, NULL),
	(5, 2, 'allow_edit', '2018-08-09 23:14:09', NULL, NULL, NULL, NULL, NULL, NULL),
	(6, 2, 'allow_delete', '2018-08-09 23:14:09', NULL, NULL, NULL, NULL, NULL, NULL),
	(7, 4, 'allow_add', '2018-08-09 23:14:09', NULL, NULL, NULL, NULL, NULL, NULL),
	(8, 4, 'allow_edit', '2018-08-09 23:14:09', NULL, NULL, NULL, NULL, NULL, NULL),
	(9, 4, 'allow_delete', '2018-08-09 23:14:09', NULL, NULL, NULL, NULL, NULL, NULL),
	(10, 5, 'allow_add', '2018-08-09 23:14:09', NULL, NULL, NULL, NULL, NULL, NULL),
	(11, 5, 'allow_edit', '2018-08-09 23:14:09', NULL, NULL, NULL, NULL, NULL, NULL),
	(12, 5, 'allow_delete', '2018-08-09 23:14:09', NULL, NULL, NULL, NULL, NULL, NULL);
/*!40000 ALTER TABLE `acc_admin_menu_item_option` ENABLE KEYS */;

-- Dumping structure for table zalon_mountain.acc_list_setting
CREATE TABLE IF NOT EXISTS `acc_list_setting` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `list_name` varchar(255) DEFAULT NULL,
  `label` varchar(255) DEFAULT '',
  `field_name` varchar(255) DEFAULT NULL,
  `type` enum('string','currency','date') DEFAULT NULL,
  `is_show` tinyint(4) DEFAULT NULL,
  `order_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `updated_by` varchar(255) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `deleted_by` varchar(255) DEFAULT NULL,
  `special_data` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table zalon_mountain.acc_list_setting: ~0 rows (approximately)
/*!40000 ALTER TABLE `acc_list_setting` DISABLE KEYS */;
/*!40000 ALTER TABLE `acc_list_setting` ENABLE KEYS */;

-- Dumping structure for table zalon_mountain.booking
CREATE TABLE IF NOT EXISTS `booking` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_id` int(11) DEFAULT NULL,
  `accept_person_id` int(11) DEFAULT NULL,
  `start_date` date DEFAULT NULL,
  `end_date` date DEFAULT NULL,
  `room_no` int(11) DEFAULT NULL,
  `adult_no` int(11) DEFAULT NULL,
  `children_no` int(11) DEFAULT NULL,
  `status` enum('pending','approve','confirm') DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(255) DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- Dumping data for table zalon_mountain.booking: ~4 rows (approximately)
/*!40000 ALTER TABLE `booking` DISABLE KEYS */;
INSERT INTO `booking` (`id`, `customer_id`, `accept_person_id`, `start_date`, `end_date`, `room_no`, `adult_no`, `children_no`, `status`, `created_at`, `created_by`, `updated_at`, `updated_by`, `deleted_at`, `deleted_by`) VALUES
	(1, 6, NULL, '2018-08-25', '2018-08-27', 2, 2, 1, 'approve', '2018-07-28 07:02:45', 'guest', '2018-08-08 17:42:06', 'guest', NULL, NULL),
	(2, 7, NULL, '2018-08-07', '2018-08-09', NULL, 6, NULL, 'pending', '2018-07-28 07:16:42', 'guest', '2018-07-28 07:16:42', NULL, NULL, NULL),
	(3, 8, NULL, '2018-05-17', '2018-05-17', 1, 2, NULL, NULL, '2018-08-08 17:48:28', 'guest', '2018-08-08 17:48:28', NULL, NULL, NULL),
	(4, 8, NULL, '2018-08-23', '2018-08-24', 1, 2, 2, 'confirm', '2018-08-08 17:50:50', 'guest', '2018-08-08 18:37:51', 'guest', NULL, NULL);
/*!40000 ALTER TABLE `booking` ENABLE KEYS */;

-- Dumping structure for table zalon_mountain.booking_room
CREATE TABLE IF NOT EXISTS `booking_room` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `room_id` int(11) DEFAULT NULL,
  `room_type_id` int(11) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `created_at` varchar(255) DEFAULT NULL,
  `created_by` datetime DEFAULT NULL,
  `updated_at` varchar(255) DEFAULT NULL,
  `updated_by` datetime DEFAULT NULL,
  `deleted_at` varchar(255) DEFAULT NULL,
  `deleted_by` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table zalon_mountain.booking_room: ~0 rows (approximately)
/*!40000 ALTER TABLE `booking_room` DISABLE KEYS */;
/*!40000 ALTER TABLE `booking_room` ENABLE KEYS */;

-- Dumping structure for table zalon_mountain.check_room_available
CREATE TABLE IF NOT EXISTS `check_room_available` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `room_id` int(11) DEFAULT NULL,
  `room_type_id` int(11) DEFAULT NULL,
  `status` tinyint(2) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(255) DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table zalon_mountain.check_room_available: ~0 rows (approximately)
/*!40000 ALTER TABLE `check_room_available` DISABLE KEYS */;
/*!40000 ALTER TABLE `check_room_available` ENABLE KEYS */;

-- Dumping structure for table zalon_mountain.customer
CREATE TABLE IF NOT EXISTS `customer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `nrc` varchar(255) DEFAULT NULL,
  `birth_date` date DEFAULT NULL,
  `gender` enum('Male','Female','Other') DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(255) DEFAULT NULL,
  `deleted-at` datetime DEFAULT NULL,
  `deleted_by` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

-- Dumping data for table zalon_mountain.customer: ~5 rows (approximately)
/*!40000 ALTER TABLE `customer` DISABLE KEYS */;
INSERT INTO `customer` (`id`, `name`, `password`, `nrc`, `birth_date`, `gender`, `phone`, `email`, `address`, `created_at`, `created_by`, `updated_at`, `updated_by`, `deleted-at`, `deleted_by`) VALUES
	(1, 'Hla', 'ggef', '12/ThaLaNa(N)12345', '2018-07-28', 'Female', '0678976546', 'hla@gmail.com', 'Yangon', '2018-07-28 06:54:30', 'guest', '2018-07-28 06:54:30', NULL, NULL, NULL),
	(2, 'Hla', '12fefefe3456', '12/ThaLaNa(N)12345', '2018-07-28', 'Female', '09689756788', 'hla@gmail.com', NULL, '2018-07-28 06:59:40', 'guest', '2018-07-28 06:59:40', NULL, NULL, NULL),
	(6, 'Hla', '123456', '12/ThaLaNa(N)12345', '2018-07-28', 'Female', '09689756788', 'hla@gmail.com', NULL, '2018-07-28 07:01:52', 'guest', '2018-07-28 07:01:52', NULL, NULL, NULL),
	(7, 'Mya', '123456', '12/BaHa(N)67867', '1991-07-18', 'Female', '09546767', 'mya@gmail.com', NULL, '2018-07-28 07:14:26', 'guest', '2018-07-28 07:14:26', NULL, NULL, NULL),
	(8, 'Aye Chan', 'asd123', 'jfdojfioj', '1994-01-09', 'Female', '0978964566', 'ayechan@gmail.com', 'Thanlyin', '2018-08-08 17:46:43', 'guest', '2018-08-08 17:46:43', NULL, NULL, NULL);
/*!40000 ALTER TABLE `customer` ENABLE KEYS */;

-- Dumping structure for table zalon_mountain.lang_list
CREATE TABLE IF NOT EXISTS `lang_list` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `abbrev` varchar(20) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `updated_by` varchar(255) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `deleted_by` varchar(255) DEFAULT NULL,
  `special_data` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- Dumping data for table zalon_mountain.lang_list: ~5 rows (approximately)
/*!40000 ALTER TABLE `lang_list` DISABLE KEYS */;
INSERT INTO `lang_list` (`id`, `name`, `abbrev`, `created_at`, `created_by`, `updated_at`, `updated_by`, `deleted_at`, `deleted_by`, `special_data`) VALUES
	(1, 'English', 'en', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(2, 'Chinese', 'zh', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(3, 'Indonesia', 'ind', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(4, 'Malaysia', 'my', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(5, 'Myanmar', 'mm', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
/*!40000 ALTER TABLE `lang_list` ENABLE KEYS */;

-- Dumping structure for table zalon_mountain.lang_presentation
CREATE TABLE IF NOT EXISTS `lang_presentation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `template` varchar(255) DEFAULT NULL,
  `en` varchar(255) DEFAULT NULL,
  `zh` varchar(255) DEFAULT NULL,
  `ind` varchar(255) DEFAULT NULL,
  `vi` varchar(255) DEFAULT NULL,
  `mm` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `updated_by` varchar(255) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `deleted_by` varchar(255) DEFAULT NULL,
  `special_data` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;

-- Dumping data for table zalon_mountain.lang_presentation: ~16 rows (approximately)
/*!40000 ALTER TABLE `lang_presentation` DISABLE KEYS */;
INSERT INTO `lang_presentation` (`id`, `template`, `en`, `zh`, `ind`, `vi`, `mm`, `created_at`, `created_by`, `updated_at`, `updated_by`, `deleted_at`, `deleted_by`, `special_data`) VALUES
	(1, 'login', 'Login', 'Login', 'Login', 'Login', 'Login', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(2, 'number', 'Number', 'Number', 'Number', 'Number', 'Number', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(3, 'search', 'Search', 'Search', 'Search', 'Search', 'Search', '2018-01-03 00:17:42', 'guest', '2018-01-03 00:17:42', NULL, NULL, NULL, NULL),
	(4, 'filter_name', 'Filter Name', 'Filter Name', 'Filter Name', 'Filter Name', 'Filter Name', '2018-01-03 00:17:42', 'guest', '2018-01-03 00:17:42', NULL, NULL, NULL, NULL),
	(5, 'active_filter', 'Active Filter', 'Active Filter', 'Active Filter', 'Active Filter', 'Active Filter', '2018-01-03 00:17:42', 'guest', '2018-01-03 00:17:42', NULL, NULL, NULL, NULL),
	(7, 'from_Date', 'From Date', 'From Date', 'From Date', 'From Date', 'From Date', '2018-01-03 00:17:43', 'guest', '2018-01-03 00:17:43', NULL, NULL, NULL, NULL),
	(8, 'to_date', 'To Date', 'To Date', 'To Date', 'To Date', 'To Date', '2018-01-03 00:17:43', 'guest', '2018-01-03 00:17:43', NULL, NULL, NULL, NULL),
	(9, 'saved_search', 'Saved Search', 'Saved Search', 'Saved Search', 'Saved Search', 'Saved Search', '2018-01-03 00:17:43', 'guest', '2018-01-03 00:17:43', NULL, NULL, NULL, NULL),
	(10, 'sort_by', 'Sort By', 'Sort By', 'Sort By', 'Sort By', 'Sort By', '2018-01-03 00:17:44', 'guest', '2018-01-03 00:17:44', NULL, NULL, NULL, NULL),
	(11, 'sort_dir', 'Sort Dir', 'Sort Dir', 'Sort Dir', 'Sort Dir', 'Sort Dir', '2018-01-03 00:17:44', 'guest', '2018-01-03 00:17:44', NULL, NULL, NULL, NULL),
	(12, 'ascending', 'Ascending', 'Ascending', 'Ascending', 'Ascending', 'Ascending', '2018-01-03 00:17:44', 'guest', '2018-01-03 00:17:44', NULL, NULL, NULL, NULL),
	(13, 'descending', 'Descending', 'Descending', 'Descending', 'Descending', 'Descending', '2018-01-03 00:17:44', 'guest', '2018-01-03 00:17:44', NULL, NULL, NULL, NULL),
	(14, 'action', 'Action', 'Action', 'Action', 'Action', 'Action', '2018-01-03 00:17:44', 'guest', '2018-01-03 00:17:44', NULL, NULL, NULL, NULL),
	(15, 'clear', 'Clear', 'Clear', 'Clear', 'Clear', 'Clear', '2018-01-03 00:17:44', 'guest', '2018-01-03 00:17:44', NULL, NULL, NULL, NULL),
	(16, 'save', 'Save', 'Save', 'Save', 'Save', 'Save', '2018-01-03 00:17:45', 'guest', '2018-01-03 00:17:45', NULL, NULL, NULL, NULL),
	(17, 'delete', 'Delete', 'Delete', 'Delete', 'Delete', 'Delete', '2018-01-03 00:17:45', 'guest', '2018-01-03 00:17:45', NULL, NULL, NULL, NULL);
/*!40000 ALTER TABLE `lang_presentation` ENABLE KEYS */;

-- Dumping structure for table zalon_mountain.room
CREATE TABLE IF NOT EXISTS `room` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `room_no` int(11) DEFAULT NULL,
  `room_type_id` int(11) DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL,
  `status` enum('free','rent') DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(255) DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8;

-- Dumping data for table zalon_mountain.room: ~29 rows (approximately)
/*!40000 ALTER TABLE `room` DISABLE KEYS */;
INSERT INTO `room` (`id`, `room_no`, `room_type_id`, `name`, `status`, `created_at`, `created_by`, `updated_at`, `updated_by`, `deleted_at`, `deleted_by`) VALUES
	(1, 101, 1, NULL, 'free', NULL, NULL, NULL, NULL, NULL, NULL),
	(2, 102, 1, NULL, 'free', NULL, NULL, NULL, NULL, NULL, NULL),
	(3, 103, 1, NULL, 'free', NULL, NULL, NULL, NULL, NULL, NULL),
	(4, 104, 1, NULL, 'free', NULL, NULL, NULL, NULL, NULL, NULL),
	(5, 105, 1, NULL, 'free', NULL, NULL, NULL, NULL, NULL, NULL),
	(6, 106, 2, NULL, 'free', NULL, NULL, NULL, NULL, NULL, NULL),
	(7, 107, 2, NULL, 'free', NULL, NULL, NULL, NULL, NULL, NULL),
	(8, 108, 3, NULL, 'free', NULL, NULL, NULL, NULL, NULL, NULL),
	(9, 201, 1, NULL, 'free', NULL, NULL, NULL, NULL, NULL, NULL),
	(10, 202, 1, NULL, 'free', NULL, NULL, NULL, NULL, NULL, NULL),
	(11, 203, 1, NULL, 'free', NULL, NULL, NULL, NULL, NULL, NULL),
	(12, 204, 2, NULL, 'free', NULL, NULL, NULL, NULL, NULL, NULL),
	(13, 205, 2, NULL, 'free', NULL, NULL, NULL, NULL, NULL, NULL),
	(14, 206, 2, NULL, 'free', NULL, NULL, NULL, NULL, NULL, NULL),
	(15, 207, 3, NULL, 'free', NULL, NULL, NULL, NULL, NULL, NULL),
	(16, 208, 3, NULL, 'free', NULL, NULL, NULL, NULL, NULL, NULL),
	(17, 301, 1, NULL, 'free', NULL, NULL, NULL, NULL, NULL, NULL),
	(18, 302, 1, NULL, 'free', NULL, NULL, NULL, NULL, NULL, NULL),
	(19, 208, 2, NULL, 'free', NULL, NULL, NULL, NULL, NULL, NULL),
	(20, 303, 2, NULL, 'free', NULL, NULL, NULL, NULL, NULL, NULL),
	(21, 304, 2, NULL, 'free', NULL, NULL, NULL, NULL, NULL, NULL),
	(22, 305, 2, NULL, 'free', NULL, NULL, NULL, NULL, NULL, NULL),
	(23, 306, 3, NULL, 'free', NULL, NULL, NULL, NULL, NULL, NULL),
	(24, 307, 3, NULL, 'free', NULL, NULL, NULL, NULL, NULL, NULL),
	(25, 308, 3, NULL, 'free', NULL, NULL, NULL, NULL, NULL, NULL),
	(26, 401, 2, NULL, 'free', NULL, NULL, NULL, NULL, NULL, NULL),
	(27, 402, 3, NULL, 'free', NULL, NULL, NULL, NULL, NULL, NULL),
	(28, 403, 3, NULL, 'free', NULL, NULL, NULL, NULL, NULL, NULL),
	(29, 409, 3, NULL, 'free', NULL, NULL, NULL, NULL, NULL, NULL),
	(30, 410, 3, NULL, 'free', NULL, NULL, NULL, NULL, NULL, NULL);
/*!40000 ALTER TABLE `room` ENABLE KEYS */;

-- Dumping structure for table zalon_mountain.room_type
CREATE TABLE IF NOT EXISTS `room_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `no_of_room` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(255) DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- Dumping data for table zalon_mountain.room_type: ~2 rows (approximately)
/*!40000 ALTER TABLE `room_type` DISABLE KEYS */;
INSERT INTO `room_type` (`id`, `name`, `no_of_room`, `created_at`, `created_by`, `updated_at`, `updated_by`, `deleted_at`, `deleted_by`) VALUES
	(1, 'Standard', 10, NULL, NULL, NULL, NULL, NULL, NULL),
	(2, 'Superior', 10, NULL, NULL, NULL, NULL, NULL, NULL),
	(3, 'Delux', 10, NULL, NULL, NULL, NULL, NULL, NULL);
/*!40000 ALTER TABLE `room_type` ENABLE KEYS */;

-- Dumping structure for table zalon_mountain.user
CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `code` varchar(255) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `mobile` varchar(255) DEFAULT NULL,
  `gender` enum('Male','Female') DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `updated_by` varchar(255) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `deleted_by` varchar(255) DEFAULT NULL,
  `special_data` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- Dumping data for table zalon_mountain.user: ~0 rows (approximately)
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` (`id`, `code`, `username`, `password`, `email`, `mobile`, `gender`, `created_at`, `created_by`, `updated_at`, `updated_by`, `deleted_at`, `deleted_by`, `special_data`) VALUES
	(1, '1', 'root', '$2y$10$wlmVNq5ClZBiMk.9ogoVJusYk8R3mjECI6SrBBMKFX1DeexOq4UNu', 'root@root.com', '96369079', 'Female', NULL, NULL, '2017-12-22 17:51:14', NULL, NULL, NULL, NULL);
/*!40000 ALTER TABLE `user` ENABLE KEYS */;

-- Dumping structure for table zalon_mountain.user_access
CREATE TABLE IF NOT EXISTS `user_access` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `acc_admin_menu_id` int(11) DEFAULT NULL,
  `acc_admin_menu_option_id` int(11) DEFAULT NULL,
  `access_type` enum('none','full','custom') DEFAULT NULL,
  `custom_access` text,
  `created_at` timestamp NULL DEFAULT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `updated_by` varchar(255) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `deleted_by` varchar(255) DEFAULT NULL,
  `special_data` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table zalon_mountain.user_access: ~0 rows (approximately)
/*!40000 ALTER TABLE `user_access` DISABLE KEYS */;
/*!40000 ALTER TABLE `user_access` ENABLE KEYS */;

-- Dumping structure for table zalon_mountain.user_session
CREATE TABLE IF NOT EXISTS `user_session` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `token` varchar(255) DEFAULT NULL,
  `ip_address` varchar(255) DEFAULT NULL,
  `login_time` datetime DEFAULT NULL,
  `logout_time` datetime DEFAULT NULL,
  `last_action` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(255) DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;

-- Dumping data for table zalon_mountain.user_session: ~15 rows (approximately)
/*!40000 ALTER TABLE `user_session` DISABLE KEYS */;
INSERT INTO `user_session` (`id`, `user_id`, `token`, `ip_address`, `login_time`, `logout_time`, `last_action`, `created_at`, `created_by`, `updated_at`, `updated_by`, `deleted_at`, `deleted_by`) VALUES
	(1, 1, 'rWZZIzYSiqHI1yNIpXWmaI2mYCrcxjAlmNUiTILl75FwqKgbob', '127.0.0.1', '2018-08-09 16:33:23', NULL, '2018-08-09 16:33:23', '2018-08-09 16:33:25', 'System', '2018-08-09 16:33:25', NULL, NULL, NULL),
	(2, 1, '7vUfyvDgw6lIpDJfdZsG6oSXb9b2Us9p7tigyaIAhQtVmoklJo', '127.0.0.1', '2018-08-09 16:53:42', NULL, '2018-08-09 16:53:42', '2018-08-09 16:53:42', 'System', '2018-08-09 16:53:42', NULL, NULL, NULL),
	(3, 1, 'EthMUVVsCtJh20LacjzdzxmFAJHHMroIcKKWQBJrTZANHchCW3', '127.0.0.1', '2018-08-11 13:11:28', NULL, '2018-08-11 13:20:20', '2018-08-11 13:11:30', 'System', '2018-08-11 13:20:20', 'guest', NULL, NULL),
	(4, 1, 'PO7VHlqxc5T0QBR3CPHg1zPFpYSbktILKx6hrVVuD9th785DNd', '127.0.0.1', '2018-08-11 13:20:23', NULL, '2018-08-11 13:29:51', '2018-08-11 13:20:24', 'System', '2018-08-11 13:29:51', 'guest', NULL, NULL),
	(5, 1, 'r3YGJv8JfSIb2TMj0pfOMQfLcPdhWTadqIcjh29r4uSOPXvAbZ', '127.0.0.1', '2018-08-11 13:20:57', NULL, '2018-08-11 13:20:57', '2018-08-11 13:20:57', 'System', '2018-08-11 13:20:57', NULL, NULL, NULL),
	(6, 1, 'Dn3VWbmyjCqjmJ70M7ZuLqeHdpxbwKiWgaHYeWHZ73fF26TNPl', '127.0.0.1', '2018-08-11 13:22:17', NULL, '2018-08-11 13:22:17', '2018-08-11 13:22:17', 'System', '2018-08-11 13:22:17', NULL, NULL, NULL),
	(7, 1, 'bxBH2dfiTQkv8MmE8qDHEaUm8EpBPojwCxc6hf7Xzw85rGSnbb', '127.0.0.1', '2018-08-11 13:27:05', NULL, '2018-08-11 13:27:05', '2018-08-11 13:27:05', 'System', '2018-08-11 13:27:05', NULL, NULL, NULL),
	(8, 1, 'zgSeVwYGy96QrKQqgJdWFwnsBUoL6bcHYvZGYFcMgPACVUK6aO', '127.0.0.1', '2018-08-11 13:28:16', NULL, '2018-08-11 13:28:16', '2018-08-11 13:28:16', 'System', '2018-08-11 13:28:16', NULL, NULL, NULL),
	(9, 1, 'LrZEjFopTa9SqStjRPA2CfxNACxYrsvF5oAQK34TJGZUdSPzk3', '127.0.0.1', '2018-08-11 13:29:14', NULL, '2018-08-11 13:29:14', '2018-08-11 13:29:14', 'System', '2018-08-11 13:29:14', NULL, NULL, NULL),
	(10, 1, 'I9BkKGoqdepK7I7scBSokkBOCpAHSArcoaaEdpIF2LijD6FN2S', '127.0.0.1', '2018-08-11 13:29:59', NULL, '2018-08-11 14:03:57', '2018-08-11 13:29:59', 'System', '2018-08-11 14:03:57', 'guest', NULL, NULL),
	(11, 1, 'LIzY7pxfcXGm97o5Lm1NhmpuUHNTkqXaAG3b9SQMrO5kbLcfhS', '127.0.0.1', '2018-08-11 14:04:01', NULL, '2018-08-11 14:15:41', '2018-08-11 14:04:03', 'System', '2018-08-11 14:15:41', 'guest', NULL, NULL),
	(12, 1, 'dbnR0915bLzDo7fBsW7Hi2a5QvzorNlkARK0NpHHS62cC1l4Z7', '127.0.0.1', '2018-08-11 14:15:45', NULL, '2018-08-11 14:20:12', '2018-08-11 14:15:45', 'System', '2018-08-11 14:20:12', 'guest', NULL, NULL),
	(13, 1, '74zB4UK0czgxkxxbZfJNfm5FYyY6i2kjsOVQrFdT5iMIh6yORk', '127.0.0.1', '2018-08-11 17:42:06', NULL, '2018-08-11 18:04:16', '2018-08-11 17:42:08', 'System', '2018-08-11 18:04:16', 'guest', NULL, NULL),
	(14, 1, 'OFdljJBfMK3iqTjdNqXMLiddEXiXvjNXacvoflh9x98hTLsTZ5', '127.0.0.1', '2018-08-11 18:04:20', NULL, '2018-08-11 18:06:39', '2018-08-11 18:04:21', 'System', '2018-08-11 18:06:39', 'guest', NULL, NULL),
	(15, 1, 'Qsev2zaBzrvB2SjPtpBbxFMWOoN2amrHmxmSos75xOyuuu2bhI', '127.0.0.1', '2018-08-11 18:06:41', NULL, '2018-08-11 18:07:08', '2018-08-11 18:06:41', 'System', '2018-08-11 18:07:08', 'guest', NULL, NULL),
	(16, 1, 'Otp2Rmj72IT6KA5FmwMGPeuHEMZ0T3MGGwAtFlbOlJkiSruyBK', '127.0.0.1', '2018-08-11 18:07:12', NULL, '2018-08-11 18:10:28', '2018-08-11 18:07:12', 'System', '2018-08-11 18:10:28', 'guest', NULL, NULL);
/*!40000 ALTER TABLE `user_session` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
