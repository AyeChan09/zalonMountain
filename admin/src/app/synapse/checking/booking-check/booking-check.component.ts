import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { HttpService } from '../../../shared/http.service';
import { TranslateService } from '@ngx-translate/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { CustomerCheckComponent } from '../customer-check/customer-check.component';
import { RoomCheckComponent } from '../room-check/room-check.component';

@Component({
  selector: 'app-booking-check',
  templateUrl: './booking-check.component.html',
  styleUrls: ['./booking-check.component.scss']
})
export class BookingCheckComponent implements OnInit {
  data: any = [];
  customerId: number;
  selectedRecord: any = {};
  currentPage: number = 1;
  totalTableData: number = 0;
  pageLimitOptions: any = [10 , 20, 30];
  pageLimit: number = this.pageLimitOptions[0];
  order: any = { "by": "created_at", "dir": "desc" };

  search: any = {};
  pagination: any = {};
  currentFunction: string = "booking";
  currentRoute: string = this.router.url;
  savedFilter: any = [];
  activeFilterId: number = 0;
  activeFilterName: string = "";
  tableHeader: any = [];

  public selections: any = {};
  constructor(
    public router: Router,
    public httpService: HttpService,
    public translate: TranslateService,
    public modalService: NgbModal
  ) { 
    this.httpService.post("/generic/get_header", { list_name: this.currentFunction }).subscribe((data) => {
      let res = data.json();
      this.tableHeader = res.data;
    });
  }

  ngOnInit() {
    this.getPage(1, 1);
  }

  getPage(page: number, filter = 0) {
    this.currentPage = page;
    this.data = this.loadData().do(res => {
      let ret = res.json();
      if (filter == 1) {
        this.totalTableData = ret.total;
      }
    })
      .map(res => res.json().data);

    return 1; //return for callback purpose, if not it'll be ignored by subsribe in confirm
  }
  loadData() {
    let param: any = {};
    param.limit = this.pageLimit;
    param.page = this.currentPage;
    return this.httpService.get("/booking/booking", param);
  }

  doCheckCustomer(customer_id) {
    let modalRef = this.modalService.open(CustomerCheckComponent);
    modalRef.componentInstance.id = customer_id;
    modalRef.result.then((result) => {
      
    });
  }

  doCheckRoom(id) {
    let modalRef = this.modalService.open(RoomCheckComponent, { size: "lg" });
    modalRef.componentInstance.customer_id = id;
    modalRef.result.then((result) => {
      if(result == 'success') {
        location.reload();
      }
    });
  }

}
