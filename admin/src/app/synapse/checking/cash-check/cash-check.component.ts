import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { HttpService } from '../../../shared/http.service';
import { TranslateService } from '@ngx-translate/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import swal from 'sweetalert2';


@Component({
  selector: 'app-cash-check',
  templateUrl: './cash-check.component.html',
  styleUrls: ['./cash-check.component.scss']
})
export class CashCheckComponent implements OnInit {
  data: any = [];
  currentPage: number = 1;
  totalTableData: number = 0;
  pageLimitOptions: any = [10 , 20, 30];
  pageLimit: number = this.pageLimitOptions[0];

  pagination: any = {};
  currentFunction: string = "cash";
  currentRoute: string = this.router.url;
  savedFilter: any = [];
  activeFilterId: number = 0;
  activeFilterName: string = "";
  tableHeader: any = [];

  constructor(
    public router: Router,
    public httpService: HttpService,
    public translate: TranslateService,
    public ngbActiveModal: NgbActiveModal,
  ) { 
    this.httpService.post("/generic/get_header", { list_name: this.currentFunction }).subscribe((data) => {
      let res = data.json();
      this.tableHeader = res.data;
    });
  }

  ngOnInit() {
    this.getPage(1, 1);
  }

  getPage(page: number, filter = 0) {
    this.currentPage = page;
    this.data = this.loadData().do(res => {
      let ret = res.json();
      if (filter == 1) {
        this.totalTableData = ret.total;
      }
    })
      .map(res => res.json().data);

    return 1; //return for callback purpose, if not it'll be ignored by subsribe in confirm
  }

  loadData() {
    let param: any = {};
    param.limit = this.pageLimit;
    param.page = this.currentPage;
    return this.httpService.get("/cash/cash", param);
  }

  confirmBooking(id) {
    let main = this;
    let title: any = this.translate.get("Notice!");
    let text: any = this.translate.get("Do you want to give this room?");
    swal({
      title: title.value,
      text: text.value,
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#0CC27E',
      cancelButtonColor: '#FF586B',
      confirmButtonText: 'Confirm',
      cancelButtonText: "Cancel"
    }).then(function (isConfirm) {
      if (isConfirm) {
        main.confirm(id);
      }
    },function(dismiss) {
      swal.close();
    });
  }

  confirm(id) {
    let rt : any = this.httpService.put("/cash/cash/"+ id);
      rt.subscribe((data) =>{
        let res = data.json();
        if(!res.error){
          let title: any = this.translate.get("Success!");
          let text: any = this.translate.get("Room reserved.");
          swal({
            title: title.value,
            text: text.value,
            type: 'success',
            confirmButtonColor: '#0CC27E',
            confirmButtonText: 'Confirm',
          }).then(function (isConfirm) {
            location.reload();
          });
          // this.doBack();
        }
    });
  }

  wrongData(id) {
    let main = this;
    let title: any = this.translate.get("Notice!");
    let text: any = this.translate.get("Are this information wrong?");
    swal({
      title: title.value,
      text: text.value,
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#0CC27E',
      cancelButtonColor: '#FF586B',
      confirmButtonText: 'Confirm',
      cancelButtonText: "Cancel"
    }).then(function (isConfirm) {
      if (isConfirm) {
        main.wrong(id);
      } 
    },function(dismiss) {
      swal.close();
    });
  }

  wrong(id) {
    let rt : any = this.httpService.post("/cash/wrong_data/"+ id);
    rt.subscribe((data) =>{
      let res = data.json();
      if(!res.error){
        // let title: any = this.translate.get("Success!");
        // let text: any = this.translate.get("Room reserved.");
        // swal({
        //   title: title.value,
        //   text: text.value,
        //   type: 'success',
        //   confirmButtonColor: '#0CC27E',
        //   confirmButtonText: 'Confirm',
        // }).then(function (isConfirm) {
        //   location.reload();
        // });
        // this.doBack();
      }
    });
  }
}
