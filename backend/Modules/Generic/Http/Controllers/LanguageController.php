<?php

namespace Modules\Generic\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;

use Illuminate\Support\Facades\Schema;
use Modules\Generic\Entities\LangPresentation;

use Carbon\Carbon;
class LanguageController extends Controller
{
    public function get_translate(){
        $lang = isset($_GET['lang'])?$_GET['lang']:'en';
        $data = DB::table('lang_presentation')->select('template',$lang)->get();
        $r=array();
        foreach ($data as $k => $v) {
            $r[$v->template] = $v->$lang;
        }
        return $r;
    }
    public function save_translate(){
        $param = isset($_GET['param'])?$_GET['param']:'';
        if($param==''){
        	return array();
        }
        $lang = array();
        $data = DB::table('lang_presentation')->where('template',trim($param))->first();
        if(!isset($data->id)){
			$en = strtolower($param);
			$en = str_replace("[[","",$en);
			$en = str_replace("]]","",$en);
			$en = str_replace("_"," ",$en);
			$en = ucwords($en);

			$lang = new LangPresentation;
			$lang->template = $param;
			$lang->en = $en;
			$lang->ind = $en;
			$lang->vi = $en;
			$lang->zh = $en; 
            $lang->mm = $en; 
			$lang->save();

        }
        return $lang;
    }


}
