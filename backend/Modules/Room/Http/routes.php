<?php

Route::group(['middleware' => [], 'prefix' => 'room', 'namespace' => 'Modules\Room\Http\Controllers'], function()
{
    Route::get('/', 'RoomController@index');
    Route::resource('/room', 'RoomController');

    Route::post("call_helper","HelperController@call");
});
